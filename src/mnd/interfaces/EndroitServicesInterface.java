/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.interfaces;

import java.util.List;
import mnd.entities.Commentaire;
import mnd.entities.Endroit;

/**
 *
 * @author Ahmed
 */
public interface EndroitServicesInterface {
    public void insertEndroit(Endroit endroit);
    public boolean updateEndroit(Endroit endroit);
    public void deleteEndroit(Endroit endroit); 
    
    public List<Endroit> listAll();
    public List<Endroit> getByUser(int user_id);
    public List<Endroit> getByCountry(String pays) ; 
    public void noteEndroit(int note); 
    public List<Commentaire> getAllComments(int endroit_id);
    public Endroit showEndroit(int endroit_id);
    
    
}
