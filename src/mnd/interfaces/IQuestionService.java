/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.interfaces;

import java.util.ArrayList;
import java.util.List;
import mnd.entities.Question;

/**
 *
 * @author Moataz
 */
public interface IQuestionService {
    
    public void addQuestion(Question q);
    public void supprimerQuestion(Question q);
    public List<Question> RechercherQuestion(String c);
    public List<Question> afficherQuestion();
    public Question afficherMaQuestion(int id);
    public ArrayList<Question> afficherMesQuestion(int id);
    public ArrayList<Question> afficherContenuQuestion();
    
}
