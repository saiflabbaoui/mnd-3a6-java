/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.interfaces;

import java.util.List;
import mnd.entities.Reponse;
import mnd.entities.Vote;

/**
 *
 * @author Moataz
 */
public interface IResponseService {
    public void addReponse(Reponse r);
    public void modifierReponse(Reponse r);
    public void supprimerReponse(Reponse r);
    public List<Reponse>afficherReponse(int i);
    public Reponse afficherMaReponse(int id);
    public void voteReponse(int id_user, int id_reponse);
    public void unvoteReponse(int id_user, int id_reponse);
    public int voteExist(int id_user, int id_reponse);
    public int countVote(int id_reponse);
    public List<Vote> AfficherNbVote(int i);
}
