/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.main;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mnd.entities.Annonce;
import mnd.services.AnnonceService;

/**
 *
 * @author Camilia
 */
public class Mnd extends Application {

    public static Stage stage = null;

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/mnd/gui/Authentification.fxml"));

        Scene scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);
        stage.setScene(scene);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    Date expiration = null;
                    AnnonceService serv = new AnnonceService();
               
                    ArrayList<Annonce> annonces = serv.getTousAnnonceLibre();
                    String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
                    Date datenow = new Date(System.currentTimeMillis());
                    for (int i = 0; i < serv.getTousAnnonceLibre().size(); i++) {
                        expiration = new SimpleDateFormat("yyyy-MM-dd").parse(annonces.get(i).getDate_expiration());
                        if (expiration.getTime() < datenow.getTime()) {
                            annonces.get(i).setStatus("e");
                        }
                        serv.ModifierAnnonce(annonces.get(i));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(Mnd.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ParseException ex) {
                    Logger.getLogger(Mnd.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }, 0, 60000);
        launch(args);
    }

}
