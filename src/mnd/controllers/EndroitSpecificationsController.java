/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfDocument;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfWriter;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXNodesList;
import com.jfoenix.controls.JFXTextField;
import com.sun.scenario.effect.ImageData;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import javax.sound.sampled.Clip;
import mnd.controllers.AuthentificationController;
import mnd.entities.Commentaire;
import mnd.entities.Endroit;
import mnd.entities.User;
import static mnd.main.Mnd.stage;
import mnd.services.CommentaireServices;
import mnd.services.EndroitServices;
import org.controlsfx.control.Rating;
/**
 * FXML Controller class
 *
 * @author Ahmed
 */
public class EndroitSpecificationsController implements Initializable {

    private JFXTextField noteDonnee;
    @FXML
    private Label address;
    @FXML
    private ImageView imageEndroit;
    private GridPane listeCom;
    @FXML
    private Label descrption;
    @FXML
    private Label note;
    @FXML
    private Label type;
    @FXML
    private Label titre;
    @FXML
    private Label type1;
    @FXML
    private Label address1;
    @FXML
    private Label nom_utilisateur;
    
    static int idEndroit ;
    static String imageUrl ;
    EndroitServices es ;
    CommentaireServices cs ;
    private JFXListView<Commentaire> listeCommentaires;
    private ObservableList<Commentaire> commData=FXCollections.observableArrayList();
    private List<User> users = new ArrayList<>();
    @FXML
    private VBox userDetails;
    @FXML
    private VBox comDetails;
    @FXML
    private HBox comContainer;
    @FXML
    private JFXTextField commentaireAjoute;
    @FXML
    private JFXButton ajoutCommentaire;
    @FXML
    private VBox dateCom;
    @FXML
    private Rating noteEndroit;
    
    @FXML
    private JFXButton modifier;
    
    
    AuthentificationController ctrl = AuthentificationController.getInstance();
    User user = ctrl.getUser();
    
    Endroit e  ;
    @FXML
    private JFXButton pdfButton;
    
    private  com.itextpdf.text.Image ImageToPdf ; 

    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        modifier.setVisible(false);
        try {
            cs = new CommentaireServices();
            
            try {
                es = new EndroitServices() ;
            } catch (SQLException ex) {
                Logger.getLogger(EndroitController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            e = es.showEndroit(idEndroit) ;
            
            if(es.getUserByEndroit(e).getId() == user.getId() ){
                modifier.setVisible(true);
            }

            setNote( String.format("%.1f", es.getNoteGlobal(e.getId_endroit())));
            String add = e.getPays() + ", " + e.getAddress();
            setAddress(add);
            setTitre(e.getTitre());
            setType(e.getType());
            nom_utilisateur.setText(es.getUserByEndroit(e).getUsername());
            setDescrption(e.getDescription());
         //   File file = new File(e.getImage_url());
        //    Image image = new Image(file.toURI().toString(), 250, 200, false, false);
            Image image = new Image(new URL("http://localhost:/pi/images/" + e.getImage_url()).openStream(),250,200,false,false);
            
            imageEndroit.setImage(image);
            imageEndroit.setFitHeight(500);
            Label imp = new Label("imprimer");
            Tooltip tooltip = new Tooltip("impri");
            Tooltip.install(imageEndroit, tooltip);
            
            
            userDetails.setAlignment(Pos.TOP_CENTER);
            userDetails.setStyle("-fx-border-style: solid;"
                + "-fx-border-width: 1;"
                + "-fx-border-color: black");
            userDetails.setPadding(new Insets(10, 50, 50, 50));
            userDetails.setSpacing(30);
            comDetails.setAlignment(Pos.TOP_CENTER);
            comDetails.setStyle("-fx-border-style: solid;"
                + "-fx-border-width: 1;"
                + "-fx-border-color: black");
            comDetails.setPadding(new Insets(10, 50, 50, 50));
            comDetails.setSpacing(30);
            dateCom.setAlignment(Pos.TOP_CENTER);
            dateCom.setStyle("-fx-border-style: solid;"
                + "-fx-border-width: 1;"
                + "-fx-border-color: black");
            dateCom.setPadding(new Insets(10, 50, 50, 50));
            dateCom.setSpacing(30);
            List<Commentaire> commentaires = new ArrayList<>();
            commentaires = es.getAllComments(idEndroit);

            for (Commentaire com :commentaires ) {
                User u = cs.getUserByComm(com);
                Label username = new Label(u.getUsername());
                Label contenu = new Label(com.getText());
                Label date = new Label(com.getDate());
               // this.username.getChildren().add(username);
                userDetails.getChildren().add(username);
                comDetails.getChildren().addAll(contenu);
                dateCom.getChildren().add(date);
            }
            

          

        } catch (SQLException ex) {
            Logger.getLogger(EndroitSpecificationsController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EndroitSpecificationsController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        noteEndroit.ratingProperty().addListener(new ChangeListener<Number>(){
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                //    Ratings r=new Ratings(mnd2.main.Mnd.getInstance().getLoggedUser().getId(), id, (double) newValue);
                noteEndroit = new Rating();
                noteEndroit.setPartialRating(true);
                noteEndroit.setUpdateOnHover(false);
                es.ajouterNote(user.getId(), idEndroit, newValue.doubleValue());
                }

        });

    }   

    public void setAddress(String txt) {
        address.setText(txt);
    }

    public void setImageEndroit(ImageView imageEndroit) {
        this.imageEndroit = imageEndroit;
    }

    public void setListeCom(GridPane listeCom) {
        this.listeCom = listeCom;
    }

    public void setDescrption(String txt) {
        descrption.setText(txt);
    }

    public void setNote(String txt) {
        note.setText(txt);
    }

    public void setTitre(String txt) {
       titre.setText(txt);
    }

    public void setType(String txt) {
        type.setText(txt);
    }

    private void noteEndroit(ActionEvent event){
        String note = noteDonnee.getText();
        
    }

    @FXML
    private void getUtilisateur(KeyEvent event) {
    }

    public static void setImageUrl(String imageUrl) {
        EndroitSpecificationsController.imageUrl = imageUrl;
    }

    @FXML
    private void showPdf(ActionEvent event) throws DocumentException {
        
            Endroit e = es.showEndroit(idEndroit);
            PrintImage print = null ; 
            print.printImage(e.getImage_url(),e.getTitre());
            
            
            
            
//            //        PDFViewer m_PDFViewer = new PDFViewer();
//            //String dest = "D:/Pdfs" + e.getImage_url().substring(6);
//            File file = new File(e.getImage_url());
//            Image image = new Image(file.toURI().toString(), 250, 200, false, false);
//            com.itextpdf.text.Image Imagepdf ;
//            
//         //   Imagepdf.setUrl(e.getImage_url());
//            
//            ImageView imgv = new ImageView(image);
//             ImageToPdf.getIn
//            
//            PdfPCell  cell = new PdfPCell();
//        //    cell.setImage(Imagepdf);
//            Document document = new Document();
//            PdfWriter.getInstance(document, new FileOutputStream("test"+".pdf"));
//            document.open();
//            document.add(cell);
//            document.close();
//            
//        PdfWriter writer;
//        writer = new PdfWriter();
//        PdfDocument pdfDoc = new PdfDocument(writer);
        
//         File file = new File(e.getImage_url());
//        Image image = new Image(file.toURI().toString(), 250, 200, false, false);
//        ImageView imagePdf = new ImageView();
//        imagePdf.setImage(image);
////        Rectangle clip = new Rectangle(
////                                imagePdf.getFitWidth(), imgview.getFitHeight()
////                        );
//        GridPane grid = new GridPane();
//        grid.getChildren().add(imagePdf);
//        m_PDFViewer.setClip(grid);

////        BorderPane borderPane = new BorderPane(m_PDFViewer);
//        Scene scene = new Scene(borderPane);
//        Stage stage1 = new Stage();
////        Hbox hbox = new Hbox();
////        Text titre = new Text(e.getTitre());
////        Text address = new Text(e.getAddress());
////        hbox.getC
////        scene.set
//        stage1.setScene(scene);
//        stage1.setTitle("JavaFX PDFViewer - Qoppa Software");
//        stage1.setScene(scene);
//        stage1.centerOnScreen();
//        stage1.show();
     
    }

    @FXML
    private void ajouterCommentaire(ActionEvent event) {
        if(commentaireAjoute.getText().equals("")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez saisir un commentaire");
            alert.showAndWait();
        }
        else {
            Commentaire c = new Commentaire(user.getId(),idEndroit,commentaireAjoute.getText());
            cs.insertComment(c);
             Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("votre commentaire a été ajouté avec succé !");
            alert.showAndWait();
        }
    }

    @FXML
    private void setRating(MouseEvent event) {
        noteEndroit.setDisable(true);
    }

    @FXML
    private void editEndroit(ActionEvent event) {
        File file = new File(e.getImage_url());
            Image image = new Image(file.toURI().toString(), 250, 200, false, false);
            imageEndroit.setImage(image);
            imageEndroit.setFitHeight(500);
        try {
            es = new EndroitServices();
            Endroit e = es.showEndroit(idEndroit);
           
            try {
                FXMLLoader loader=new FXMLLoader(getClass().getResource("/mnd/gui/ModifierEndroit.fxml"));
                Parent root=loader.load();
                ModifierEndroitController ctrl = loader.getController();
 
                ctrl.setTitre(e.getTitre());
                ctrl.setType(e.getType());
                ctrl.setPays(e.getPays());
                ctrl.setAddress(e.getAddress());
                ctrl.setDescription(e.getDescription());
                System.out.println(e.getImage_url());
                ctrl.setEndroitImage(e.getImage_url());
                ctrl.idEndroit = e.getId_endroit() ;
                ctrl.setImageUrl(e.getImage_url());
                
                
                
                Scene newScene = new Scene(root);
                Stage newStage = new Stage();
                newStage.setTitle("Informations");
                newStage.setScene(newScene);
                newStage.show();
            } catch (IOException ex) {
                Logger.getLogger(EndroitSpecificationsController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EndroitSpecificationsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void showPrintLabel(MouseDragEvent event) {
        pdfButton.setVisible(true);
    }
    
    
    
}
