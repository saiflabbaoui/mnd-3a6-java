/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import com.jfoenix.controls.JFXComboBox;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import mnd.entities.Annonce;
import mnd.entities.User;
import mnd.services.AnnonceService;
import mnd.services.UserService;

/**
 * FXML Controller class
 *
 * @author kaisf
 */
public class RechercherAnnonceController implements Initializable {

    private final ObservableList<String> listePays = FXCollections.observableArrayList("", "Afghanistan", " Albanie", " Antarctique", " Algérie", " Samoa Américaines", " Andorre", " Angola", " Antigua-et-Barbuda", " Azerbaïdjan", " Argentine", " Australie", " Autriche", " Bahamas", " Bahreïn", " Bangladesh", " Arménie", " Barbade", " Belgique", " Bermudes", " Bhoutan", " Bolivie", " Bosnie-Herzégovine", " Botswana", " Île Bouvet", " Brésil", " Belize", " Territoire Britannique de Océan Indien", " Îles Salomon", " Îles Vierges Britanniques", " Brunéi Darussalam", " Bulgarie", " Myanmar", " Burundi", " Bélarus", " Cambodge", " Cameroun", " Canada", " Cap-vert", " Îles Caïmanes", " République Centrafricaine", " Sri Lanka", " Tchad", " Chili", " Chine", " Taïwan", " Île Christmas", " Îles Cocos (Keeling)", " Colombie", " Comores", " Mayotte", " République du Congo", " République Démocratique du Congo", " Îles Cook", " Costa Rica", " Croatie", " Cuba", " Chypre", " République Tchèque", " Bénin", " Danemark", " Dominique", " République Dominicaine", " Équateur", " El Salvador", " Guinée Équatoriale", " Éthiopie", " Érythrée", " Estonie", " Îles Féroé", " Îles (malvinas) Falkland", " Géorgie du Sud et les Îles Sandwich du Sud", " Fidji", " Finlande", " Îles Åland", " France", " Guyane Française", " Polynésie Française", " Terres Australes Françaises", " Djibouti", " Gabon", " Géorgie", " Gambie", " Territoire Palestinien Occupé", " Allemagne", " Ghana", " Gibraltar", " Kiribati", " Grèce", " Groenland", " Grenade", " Guadeloupe", " Guam", " Guatemala", " Guinée", " Guyana", " Haïti", " Îles Heard et Mcdonald", " Saint-Siège (état de la Cité du Vatican)", " Honduras", " Hong-Kong", " Hongrie", " Islande", " Inde", " Indonésie", " République Islamique Iran", " Iraq", " Irlande", " Israël", " Italie", " Côte Ivoire", " Jamaïque", " Japon", " Kazakhstan", " Jordanie", " Kenya", " République Populaire Démocratique de Corée", " République de Corée", " Koweït", " Kirghizistan", " République Démocratique Populaire Lao", " Liban", " Lesotho", " Lettonie", " Libéria", " Jamahiriya Arabe Libyenne", " Liechtenstein", " Lituanie", " Luxembourg", " Macao", " Madagascar", " Malawi", " Malaisie", " Maldives", " Mali", " Malte", " Martinique", " Mauritanie", " Maurice", " Mexique", " Monaco", " Mongolie", " République de Moldova", " Montserrat", " Maroc", " Mozambique", " Oman", " Namibie", " Nauru", " Népal", " Pays-Bas", " Antilles Néerlandaises", " Aruba", " Nouvelle-Calédonie", " Vanuatu", " Nouvelle-Zélande", " Nicaragua", " Niger", " Nigéria", " Niué", " Île Norfolk", " Norvège", " Îles Mariannes du Nord", " Îles Mineures Éloignées des États-Unis", " États Fédérés de Micronésie", " Îles Marshall", " Palaos", " Pakistan", " Panama", " Papouasie-Nouvelle-Guinée", " Paraguay", " Pérou", " Philippines", " Pitcairn", " Pologne", " Portugal", " Guinée-Bissau", " Timor-Leste", " Porto Rico", " Qatar", " Réunion", " Roumanie", " Fédération de Russie", " Rwanda", " Sainte-Hélène", " Saint-Kitts-et-Nevis", " Anguilla", " Sainte-Lucie", " Saint-Pierre-et-Miquelon", " Saint-Vincent-et-les Grenadines", " Saint-Marin", " Sao Tomé-et-Principe", " Arabie Saoudite", " Sénégal", " Seychelles", " Sierra Leone", " Singapour", " Slovaquie", " Viet Nam", " Slovénie", " Somalie", " Afrique du Sud", " Zimbabwe", " Espagne", " Sahara Occidental", " Soudan", " Suriname", " Svalbard etÎle Jan Mayen", " Swaziland", " Suède", " Suisse", " République Arabe Syrienne", " Tadjikistan", " Thaïlande", " Togo", " Tokelau", " Tonga", " Trinité-et-Tobago", " Émirats Arabes Unis", " Tunisie", " Turquie", " Turkménistan", " Îles Turks et Caïques", " Tuvalu", " Ouganda", " Ukraine", "ex-République Yougoslave de Macédoine", " Égypte", " Royaume-Uni", " Île de Man", " République-Unie de Tanzanie", " États-Unis", " Îles Vierges des États-Unis", " Burkina Faso", " Uruguay", " Ouzbékistan", " Venezuela", " Wallis et Futuna", " Samoa", " Yémen", " Serbie-et-Monténégro", " Zambie");

    User u = AuthentificationController.getInstance().getUser();
    int page = 0;
    int max;
    public static RechercherAnnonceController r;
    private int id;
    ArrayList<Annonce> annonces;
    ArrayList<Annonce> annoncesfiltred;
    private Label description;
    @FXML
    private TextField age;
    private Label sexe;
    private Label status;
    private Label error;
    @FXML
    private DatePicker expiration;
    @FXML
    private JFXComboBox<String> pays;
    @FXML
    private GridPane grid;
    @FXML
    private CheckBox masculin;
    @FXML
    private CheckBox feminin;
    @FXML
    private TextField personne;
    @FXML
    private AnchorPane ap;
    @FXML
    private Button retour;
    @FXML
    private Button suivant;
    @FXML
    private Button filtrer;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            pays.setItems(listePays);
            AnnonceService serv = new AnnonceService();
            annonces = serv.getTousAnnonceLibre();
            max = annonces.size();
            updateGrid(annonces);

            // TODO
        } catch (SQLException ex) {
            Logger.getLogger(ReservationController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RechercherAnnonceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void filter(ActionEvent event) throws SQLException, FileNotFoundException {
        annoncesfiltred = new ArrayList<Annonce>();
        String sexe;
        if (masculin.selectedProperty().get() == true && feminin.selectedProperty().get() == true) {
            sexe = "both";
        } else if (masculin.selectedProperty().get() == true) {
            sexe = "masculin";
        } else if (feminin.selectedProperty().get() == true) {
            sexe = "feminin";
        } else {
            sexe = "";
        }
        for (int i = 0; i < annonces.size(); i++) {
            if (pays.getValue() == null || pays.getValue().equals("") || pays.getValue().equalsIgnoreCase(annonces.get(i).getPays())) {
                if (personne.getText().equals("") || Integer.parseInt(personne.getText()) == annonces.get(i).getNb_personne()) {
                    if (age.getText().equals("") || !isInteger(age.getText(), 10) || Integer.parseInt(age.getText()) >= annonces.get(i).getAge()) {
                        if (sexe.equals("") || sexe.equals(annonces.get(i).getSexe())) {
                            if (expiration.getValue() == null || java.sql.Date.valueOf(expiration.getValue()).getTime() <= java.sql.Date.valueOf(annonces.get(i).getDate_expiration()).getTime()) {
                                annoncesfiltred.add(annonces.get(i));
                            }
                        }
                    }
                }
            }
        }
        max = annoncesfiltred.size();
        updateGrid(annoncesfiltred);
    }

    private void updateGrid(ArrayList<Annonce> annonces) throws FileNotFoundException {
        try {
            UserService userserv = new UserService();
            grid.getChildren().clear();
            for (int i = 0 + 12 * page; i < annonces.size() && i < 12 + 12 * page; i++) {
                final Label f = new Label(userserv.findUserById(annonces.get(i).getId_user()).getUsername());
                final int id_annonce = annonces.get(i).getId();
                f.setStyle("-fx-text-fill:#100BA4;-fx-font-size: 13pt;");
                f.setOnMouseEntered(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent arg0) {
                        f.setStyle("-fx-text-fill:#5381A4;-fx-underline: true;-fx-font-size: 13pt;");
                    }
                });
                f.setOnMouseExited(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent arg0) {
                        f.setStyle("-fx-text-fill:#100BA4;-fx-underline: false;-fx-font-size: 13pt;");
                    }
                });
                f.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent arg0) {
                        id = id_annonce;
                        try {
                            Parent root2 = FXMLLoader.load(getClass().getResource("/mnd/gui/ReservationAnnonce.fxml"));
                            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/mnd/gui/Ui.fxml"));
                            Parent root = (Parent) fxmlLoader.load();
                            UiController controller = fxmlLoader.<UiController>getController();
                            controller.setParent(root2);
                            Scene scene = new Scene(root);
                            Stage stage = (Stage) ap.getScene().getWindow();
                            stage.setScene(scene);
                            stage.show();
                        } catch (IOException ex) {
                            Logger.getLogger(RechercherAnnonceController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }

                });
                String photourl = userserv.findUserById(annonces.get(i).getId_user()).getPhoto_membre();
                Image image;
                if (photourl == null || photourl == "") {
                    image = new Image(new URL("http://localhost:/pi/images/default.jpg").openStream());
                } else {
                    image = new Image(new URL("http://localhost:/pi/images/" + photourl).openStream());
                }
                Circle cir2 = new Circle(45, 45, 45);
                cir2.setStroke(Color.SEAGREEN);
                cir2.setFill(new ImagePattern(image));
                cir2.setEffect(new DropShadow(+25d, 0d, +2d, Color.DARKSEAGREEN));
                GridPane.setConstraints(cir2, 0 + 2 * (i % 3), (i - 12 * page) / 3);
                grid.getChildren().add(cir2);
                GridPane.setConstraints(f, 1 + 2 * (i % 3), (i - 12 * page) / 3);
                grid.getChildren().add(f);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RechercherAnnonceController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RechercherAnnonceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public RechercherAnnonceController() {
        r = this;
    }

    public static RechercherAnnonceController getInstance() {
        return r;
    }

    public int getid() {
        return id;
    }

    @FXML
    private void next(ActionEvent event) throws FileNotFoundException {
        if ((page + 1) * 12 < max) {
            page += 1;
            if (annoncesfiltred == null) {
                updateGrid(annonces);
            } else {
                updateGrid(annoncesfiltred);
            }
        }
    }

    @FXML
    private void previous(ActionEvent event) throws FileNotFoundException {
        if (page > 0) {
            page -= 1;
            if (annoncesfiltred == null) {
                updateGrid(annonces);
            } else {
                updateGrid(annoncesfiltred);
            }
        }
    }

    private void ajouterAnnonce(ActionEvent event) throws SQLException, FileNotFoundException, IOException {
        Parent root = null;
        root = FXMLLoader.load(getClass().getResource("/mnd/gui/AjouterAnnonce.fxml"));
        Stage stage2 = new Stage();
        Scene newScene = new Scene(root);
        stage2.setScene(newScene);
        stage2.show();
    }

    public static boolean isInteger(String s) {
        return isInteger(s, 10);
    }

    public static boolean isInteger(String s, int radix) {
        if (s.isEmpty()) {
            return false;
        }
        for (int i = 0; i < s.length(); i++) {
            if (i == 0 && s.charAt(i) == '-') {
                if (s.length() == 1) {
                    return false;
                } else {
                    continue;
                }
            }
            if (Character.digit(s.charAt(i), radix) < 0) {
                return false;
            }
        }
        return true;
    }
}
