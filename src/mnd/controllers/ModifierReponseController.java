/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import mnd.entities.Reponse;
import static mnd.controllers.ReponseMenuController.reponse_id;
import static mnd.controllers.ReponseMenuController.s;
import mnd.services.ReponseService;

/**
 * FXML Controller class
 *
 * @author Moataz
 */
public class ModifierReponseController implements Initializable {

    @FXML
    private Label reponselb;
    @FXML
    private JFXTextField contenu;
    @FXML
    private JFXButton modifier;
     
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
       ReponseService rs = new ReponseService(); 
       Reponse r = rs.afficherMaReponse(reponse_id);
       contenu.setText(r.getContenu());
       
       
    }    

    @FXML
    private void modifierAction(ActionEvent event) {
        ReponseService rs = new ReponseService();
        
        Reponse r = rs.afficherMaReponse(reponse_id);
        if(!contenu.getText().equals("")){
        r.setContenu(contenu.getText());
        rs.modifierReponse(r);
        s.close();

        }else{
           Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez saisir une reponse");
            alert.showAndWait();

        }
    }
    } 
    

