/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import com.itextpdf.text.BadElementException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Ahmed
 */
public class PrintImage {
    public static void printImage(String url, String titre) throws DocumentException{
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("C:/test.pdf"));
            document.open();
       //     javafx.scene.image.Image image = new javafx.scene.image.Image(new URL("http://localhost:/pi/images/" + url).openStream(),250,200,false,false);
            Image img = Image.getInstance(new URL("http://localhost:/pi/images/" + url));
            document.add(new Paragraph("Nom de l'endroit : " + titre));
            img.scaleAbsolute(500, 300);
            document.add(img);
            document.close();
            Desktop.getDesktop().open(new File("C:/test.pdf"));
            System.out.println("Done");
        } catch (BadElementException ex) {
            Logger.getLogger(PrintImage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PrintImage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
