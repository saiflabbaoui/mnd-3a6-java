/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.regex.Pattern;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import mnd.controllers.AuthentificationController;
import mnd.entities.User;
import mnd.services.UserService;

/**
 * FXML Controller class
 *
 * @author Camilia
 */
public class ModifierProfileController implements Initializable {

    User u = AuthentificationController.getInstance().getUser();
    @FXML
    private Label textLabel;
    @FXML
    private ChoiceBox pays;
    @FXML
    private JFXButton ajouterImage;
    @FXML
    private JFXButton submitAjout;
    @FXML
    private ImageView endroitImage;

    private final ObservableList<String> listePays = FXCollections.observableArrayList("Tunisie", "France", "Espagne", "Allemagne", "Turquie", "USA", "Angleterre");
    @FXML
    private JFXTextField nom;
    @FXML
    private JFXTextField age;
    @FXML
    private JFXTextField email;
    @FXML
    private JFXTextField number;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        pays.setValue("Tunisie");
        pays.setItems(listePays);
      

    }

    @FXML
    private void handleUpload(ActionEvent event) {
    }

    @FXML
    private void modifierProfil(ActionEvent event) throws SQLException {
        int id = AuthentificationController.getInstance().getUser().getId();

        UserService us = new UserService();
        User user = us.findUserById(id);
        String p = (String) pays.getValue();
        String name = nom.getText();
        int ageS = Integer.parseInt(age.getText());
        int numero = Integer.parseInt(number.getText());
        String emaile = email.getText();
         if (name.length() > 0 && emaile.length() > 0 ) {
            if (us.findUserByUsername(name) == null) {
                if (ValidateEmail(emaile)&& (us.findUserByEmail(emaile)==null || emaile==u.getEmail())) {                                    
                      if (isInteger(age.getText())&& ageS<100 && ageS>8) {
 
        User u = new User(p, name, ageS, numero, emaile);
        u.setId(id);
        us.updateUser(u);

        Stage stage = (Stage) submitAjout.getScene().getWindow();
        stage.close();
                      }
                }
            }
         }

    }

    @FXML
    private void acueilReturn(MouseEvent event) {
        Stage stage = (Stage) submitAjout.getScene().getWindow();
        stage.close();

    }
    public static boolean isInteger(String s) {
    return isInteger(s,10);
}

public static boolean isInteger(String s, int radix) {
    if(s.isEmpty()) return false;
    for(int i = 0; i < s.length(); i++) {
        if(i == 0 && s.charAt(i) == '-') {
            if(s.length() == 1) return false;
            else continue;
        }
        if(Character.digit(s.charAt(i),radix) < 0) return false;
    }
    return true;
}
   public Boolean ValidateEmail(String email) {
        String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(regex);
        return pattern.matcher(email).matches();

    }




}
