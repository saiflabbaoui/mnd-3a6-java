/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.scene.image.Image;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Observable;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeTableColumn.CellDataFeatures;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ZoomEvent;
import javafx.util.Duration;
import mnd.controllers.AuthentificationController;
import static mnd.controllers.UiController.borderPaneS;
import mnd.entities.Question;
import mnd.entities.Reponse;
import mnd.entities.User;
import mnd.services.QuestionService;
import mnd.services.ReponseService;
import mnd.services.UserService;
import org.controlsfx.control.Notifications;
import org.controlsfx.control.textfield.TextFields;

/**
 * FXML Controller class
 *
 * @author Moataz
 */
public class QuestionMenuController implements Initializable {

    private TextArea questionArea;
    User nu;
    User u = AuthentificationController.getInstance().getUser();
    UserService us;
    @FXML
    private TableView<Question> tableQuestion;
    @FXML
    private TableColumn<Question, String> question;
    @FXML
    private TableColumn<Question, String> date;
    @FXML
    private TableColumn<Question, String> user;
    @FXML
    private TableColumn<Question, String> nb_reponse;

    private ObservableList<Question> list;
    int questionSelected;
    public static int question_id;
    @FXML
    private Button valider;
    @FXML
    private TextField rechercher;
    @FXML
    private JFXTextField questionField;
    @FXML
    private JFXTextField google;
    @FXML
    private JFXButton googleA;
    @FXML
    private ImageView googleImage;
    @FXML
    private JFXButton afficherQ;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Image i = new Image(new File("C:/Users/Moataz/Desktop/G.png").toURI().toString());
        googleImage.setImage(i);
        refresh();
    }

    @FXML
    private void validerQuestion(ActionEvent event) {
        QuestionService qs = new QuestionService();

        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        String contenu = questionField.getText();
        if (contenu.equals("")) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez saisir un question");
            alert.showAndWait();
        } else {
            Question q = new Question(u.getId(), contenu, timeStamp);
            qs.addQuestion(q);
            Notifications notificationBuilder = Notifications.create()
                    .title("Question")
                    .text("votre question a eté ajouté")
                    .graphic(null)
                    .hideAfter(Duration.seconds(5))
                    .position(Pos.TOP_RIGHT);
            notificationBuilder.darkStyle();
            notificationBuilder.showInformation();
            refresh();
        }
    }

    public void refresh() {
        try {
            QuestionService qs = new QuestionService();
            UserService t = new UserService();
            ReponseService r = new ReponseService();
            ArrayList<Question> q = new ArrayList<Question>();
            q = qs.afficherContenuQuestion();
            TextFields.bindAutoCompletion(rechercher, q);
            TextFields.bindAutoCompletion(questionField, q);
            list = FXCollections.observableArrayList(qs.afficherQuestion());
//            list.add(list.get(list.size() - 1));
            question.setCellValueFactory(new PropertyValueFactory<>("contenu"));
            date.setCellValueFactory(new PropertyValueFactory<>("date_publication"));
            nb_reponse.setCellValueFactory(i -> {
                int b = r.afficherReponse(i.getValue().getId()).size();
                SimpleStringProperty aa = new SimpleStringProperty(Integer.toString(b));
                return aa;
            });
            user.setCellValueFactory(i -> {
                SimpleStringProperty a = new SimpleStringProperty(t.findUserById(i.getValue().getId_user()).getUsername());
                return a;
            });
            tableQuestion.setItems(list);
        } catch (SQLException ex) {
            Logger.getLogger(QuestionMenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void getQuestion(MouseEvent event) throws IOException {
        questionSelected = tableQuestion.getSelectionModel().getSelectedItem().getId();

        question_id = questionSelected;

        Parent root = FXMLLoader.load(getClass().getResource("/mnd/gui/ReponseMenu.fxml"));
        borderPaneS.setCenter(root);
    }

    @FXML
    private void rechercherAction(KeyEvent event) {
        try {
            UserService t = new UserService();
            ReponseService r = new ReponseService();
            String c = rechercher.getText();
            QuestionService qs = new QuestionService();
            list = FXCollections.observableArrayList(qs.RechercherQuestion(c));
            question.setCellValueFactory(new PropertyValueFactory<>("contenu"));
            date.setCellValueFactory(new PropertyValueFactory<>("date_publication"));
            nb_reponse.setCellValueFactory(i -> {
                int b = r.afficherReponse(i.getValue().getId()).size();
                SimpleStringProperty aa = new SimpleStringProperty(Integer.toString(b));
                return aa;
            });
            user.setCellValueFactory(i -> {
                SimpleStringProperty a = new SimpleStringProperty(t.findUserById(i.getValue().getId_user()).getUsername());
                return a;
            });
            tableQuestion.setItems(list);
        } catch (SQLException ex) {
            Logger.getLogger(QuestionMenuController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void googleAction(ActionEvent event) throws IOException {
        String a = google.getText().replace(" ", " ");
        java.awt.Desktop.getDesktop().browse(java.net.URI.create("http://www.google.com/search?hl=en&q=" + a + "&btnG=Google+Search"));
    }

    @FXML
    private void mesQuestionAction(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/mnd/gui/MesQuestion.fxml"));
        borderPaneS.setCenter(root);
    }

}
