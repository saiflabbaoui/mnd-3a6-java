/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.activation.MimetypesFileTypeMap;
import mnd.controllers.AuthentificationController;
import mnd.entities.Endroit;
import mnd.entities.User;
import static mnd.main.Mnd.stage;
import mnd.services.EndroitServices;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 * FXML Controller class
 *
 * @author Ahmed
 */
public class AjoutEndroitController implements Initializable {
    @FXML
    private JFXTextField titre;
    @FXML
    private JFXTextField address;
    @FXML
    private ChoiceBox<String> pays;
    @FXML
    private JFXTextArea description;
    @FXML
    private JFXButton ajouterImage;
    @FXML
    private JFXButton submitAjout;
    @FXML
    private ImageView endroitImage;
    @FXML
    private ChoiceBox<String> type;
    
    EndroitServices es ;
    private final ObservableList<String> listeType = FXCollections.observableArrayList("Restaurant", "Café", "Bar", "Parc", "Fast-Food");
    private final ObservableList<String> listePays = FXCollections.observableArrayList("Tunisie", "France", "Espagne", "Allemagne", "Turquie", "USA", "Angleterre");
    private String imageUrl ;
    @FXML
    private Label textLabel;
    
    AuthentificationController ctrl = AuthentificationController.getInstance();
    User user = ctrl.getUser();
    @FXML
    private AnchorPane ap;
    

    /**
     * Initializes the controller class.
     */
    @FXML
    private void handleUpload(ActionEvent t) throws IOException{
        
        ImageView imgView = new ImageView();//setting imageview where we will set our uploaded picture before taking it to the database
        FileChooser fc = new FileChooser();
//        FileChooser.ExtensionFilter ext1 = new FileChooser.ExtensionFilter("JPG files(.jpg)", ".JPG");
//        FileChooser.ExtensionFilter ext2 = new FileChooser.ExtensionFilter("PNG files(.png)", ".PNG");
//        fc.getExtensionFilters().addAll(ext1, ext2);
        Stage primarystage = (Stage) ap.getScene().getWindow();
        File source = fc.showOpenDialog(primarystage);
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpPost httppost = new HttpPost("http://localhost/pi/upload.php");
            FileBody bin = new FileBody(source);
            HttpEntity reqEntity = MultipartEntityBuilder.create()
                    .addPart("fileToUpload", bin)
                    .build();

            httppost.setEntity(reqEntity);
            CloseableHttpResponse response = httpclient.execute(httppost);
            try {
                String mimetype = new MimetypesFileTypeMap().getContentType(source);
                String type = mimetype.split("/")[0];
//                if (type.equals("image")) {
                    imageUrl = source.getName();
                    System.out.println(imageUrl);
                    Image image = new Image(new URL("http://localhost:/pi/images/" + imageUrl).openStream());
                    endroitImage.setImage(image);
//                }
                
                HttpEntity resEntity = response.getEntity();
                EntityUtils.consume(resEntity);

            } finally {
                response.close();
            }
        } finally {
            httpclient.close();
        }

        
//        FileChooser fileChooser = new FileChooser();
//
//        File file = fileChooser.showOpenDialog(stage);
//        if (file != null) {
//            try {
//                imageUrl = file.toURI().toURL().toExternalForm();
//                Image image = new Image(imageUrl);
//                endroitImage.setImage(image);
//            } catch (MalformedURLException ex) {
//                throw new IllegalStateException(ex);
//            }
//        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            es = new EndroitServices() ;
        } catch (SQLException ex) {
            Logger.getLogger(AjoutEndroitController.class.getName()).log(Level.SEVERE, null, ex);
        }
        pays.setItems(listePays);
        type.setItems(listeType);
        
        
        
    }    

    @FXML
    private void ajoutEndroit(ActionEvent event) {
        String eTitre = titre.getText();
        String eAddress = address.getText();
        String ePays = (String) pays.getValue();
        String eDesc = description.getText();
        String eType = (String) type.getValue();
        
        
        
        if(eTitre.equals("")){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez remplir le champ titre");
            alert.showAndWait();
        }
        else if( type.getValue()== null ){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez remplir le champ type");
            alert.showAndWait();
        }
        else if( pays.getValue() == null ){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez remplir le champ pays");
            alert.showAndWait();
        }
        else if(eAddress.equals("")){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez remplir le champ addresse");
            alert.showAndWait();
        }  
        else if(es.ifTitreExists(eTitre)){
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Cet endroit existe déja");
            alert.showAndWait();
        }
        else 
        {
            String url = imageUrl;
            Endroit e = new Endroit(user.getId(),eTitre,url,eDesc,ePays,eAddress,eType);
            es.insertEndroit(e);
            System.out.println("ajout effectué !" + e);
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Ajout effectué avec succés !");
            alert.showAndWait();
            EndroitController ctrl = new EndroitController();
           // ctrl.listEndroits.refresh();
        }
    }

    public String getImageUrl() {
        return imageUrl;
    }
    
    
    
           
    
    
}
