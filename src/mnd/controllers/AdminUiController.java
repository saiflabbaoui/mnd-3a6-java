/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mnd.controllers.AuthentificationController;
import mnd.entities.User;
import mnd.services.UserService;

/**
 * FXML Controller class
 *
 * @author Camilia
 */
public class AdminUiController implements Initializable {

@FXML
    private Label profileLabel;
    @FXML
    private BorderPane borderPane;
    User u = AuthentificationController.getInstance().getUser();
    UserService us;
    @FXML
    private HBox upbox;
    @FXML
    private FontAwesomeIcon log;
    @FXML
    private FontAwesomeIcon logout;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        profileLabel.setText(u.getUsername());
       
    }    

    @FXML
    private void acueilBtn(MouseEvent event) throws IOException {
         Parent root=null;
         root = FXMLLoader.load(getClass().getResource("/mnd/gui/AdminHome.fxml"));
         borderPane.setCenter(root);
    }
    @FXML
    private void endroitBtn(MouseEvent event) {
    }

    @FXML
    private void annonceBtn(MouseEvent event) throws IOException {
                 Parent root=null;
         root = FXMLLoader.load(getClass().getResource("/mnd/gui/AdminAnnonce.fxml"));
         borderPane.setCenter(root);
        
    }

    @FXML
    private void evenementBtn(MouseEvent event) {
    }


    @FXML
    private void qnaBtn(MouseEvent event) {
    }

    @FXML
    private void disconnect(MouseEvent event) throws IOException {
         Stage stage = (Stage) log.getScene().getWindow();
         Parent root = FXMLLoader.load(getClass().getResource("/mnd/gui/Authentification.fxml"));
                            stage.close();
                            stage = new Stage();
                            Scene scene = new Scene(root);
                            scene.setFill(Color.TRANSPARENT);
                            stage.setScene(scene);
                            stage.initStyle(StageStyle.TRANSPARENT);
                            stage.show();
    }

    @FXML
    private void quit(MouseEvent event) {
          Stage stage = (Stage) logout.getScene().getWindow();
        stage.close();
    }
    
}