/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import mnd.controllers.AuthentificationController;
import mnd.entities.Endroit;
import mnd.entities.User;
import static mnd.main.Mnd.stage;
import mnd.services.EndroitServices;

/**
 * FXML Controller class
 *
 * @author Ahmed
 */
public class ModifierEndroitController implements Initializable {

    @FXML
    private JFXTextField titre;
    @FXML
    private JFXTextField address;
    @FXML
    private Label textLabel;
    @FXML
    private ChoiceBox<String> pays;
    @FXML
    private JFXTextArea description;
    @FXML
    private JFXButton ajouterImage;
    private JFXButton submitAjout;
    @FXML
    private ImageView endroitImage;
    @FXML
    private ChoiceBox<String> type;
    
    private String imageUrl ;
    static int idEndroit ;
    
    
    
    private final ObservableList<String> listeType = FXCollections.observableArrayList("Restaurant", "Café", "Bar", "Parc", "Fast-Food");
    private final ObservableList<String> listePays = FXCollections.observableArrayList("Tunisie", "France", "Espagne", "Allemagne", "Turquie", "USA", "Angleterre");
    
    AuthentificationController ctrl = AuthentificationController.getInstance();
    User user = ctrl.getUser();
    @FXML
    private JFXButton submitModif;
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        pays.setItems(listePays);
        type.setItems(listeType);
        System.out.println(imageUrl);

        
    }    

    @FXML
    private void handleUpload(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            try {
                imageUrl = file.toURI().toURL().toExternalForm();
                Image image = new Image(imageUrl);
                endroitImage.setImage(image);
            } catch (MalformedURLException ex) {
                throw new IllegalStateException(ex);
            }
        }
    }


    public void setTitre(String txt) {
        titre.setText(txt);
    }

    public void setAddress(String txt) {
        address.setText(txt);
    }

    public void setTextLabel(String txt) {
        textLabel.setText(txt);
    }

    public void setPays(String p) {
        pays.setValue(p);
    }

    public void setDescription(String txt) {
        description.setText(txt);
    }

    public void setAjouterImage(JFXButton ajouterImage) {
        this.ajouterImage = ajouterImage;
    }

    public void setSubmitAjout(JFXButton submitAjout) {
        this.submitAjout = submitAjout;
    }

    public void setEndroitImage(String url) throws IOException {
        
        try {
            // File file = new File(url);
            
            Image image = new Image(new URL("http://localhost:/pi/images/" + url).openStream(), 250, 200, false, false);
            endroitImage.setImage(image);
        } catch (MalformedURLException ex) {
            Logger.getLogger(ModifierEndroitController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public void setType(String t) {
        type.setValue(t);
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
    

    @FXML
    private void modifierEndroit(ActionEvent event) throws SQLException {
//        String eTitre = titre.getText();
//        String eAddress = address.getText();
//        String ePays = (String) pays.getValue();
//        String eDesc = description.getText();
//        String eType = (String) type.getValue();
//        
        EndroitServices es = new EndroitServices();
         String nvTitre = titre.getText();
        String nvAddress = address.getText();
        String nvPays = pays.getValue().toString();
        String nvType = type.getValue().toString();
        String nvDescription =description.getText();
        String nvUrl = imageUrl ;
        System.out.println(imageUrl);
        
        if(nvTitre.equals("")){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez remplir le champ titre");
            alert.showAndWait();
        }
        else if( type.getValue()== null ){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez remplir le champ type");
            alert.showAndWait();
        }
        else if( pays.getValue() == null ){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez remplir le champ pays");
            alert.showAndWait();
        }
        else if(nvAddress.equals("")){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez remplir le champ addresse");
            alert.showAndWait();
        }  
        else 
        {
           // String nurl = nvUrl.substring(6);
           
            Endroit e = new Endroit(idEndroit,user.getId(),nvTitre, nvUrl, nvDescription, nvPays, nvAddress, nvType);
            if(es.updateEndroit(e)){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Modification Endroit");
                alert.setHeaderText(null);
                alert.setContentText("Modification effectué avec succés !");
                alert.showAndWait();
                EndroitController ctrl = new EndroitController();
                ctrl.refreshList(ctrl.endroits);
            };
           
        }
       
        
        
         
    }

    @FXML
    private void deleteEndroit(MouseEvent event) {
        try {
            EndroitServices es = new EndroitServices();
            Endroit e = es.showEndroit(idEndroit);
            es.deleteEndroit(e);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Suppression Endroit");
            alert.setHeaderText(null);
            alert.setContentText("L'endroit a été supprimé");
            alert.showAndWait();
            EndroitController ctrl = new EndroitController();
                ctrl.refreshList(ctrl.endroits);
        } catch (SQLException ex) {
            Logger.getLogger(ModifierEndroitController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
}
