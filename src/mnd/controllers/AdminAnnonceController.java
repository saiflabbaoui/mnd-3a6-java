/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.PieChart.Data;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import mnd.entities.Annonce;
import mnd.services.AnnonceService;
import mnd.services.UserService;

/**
 * FXML Controller class
 *
 * @author kaisf
 */
public class AdminAnnonceController implements Initializable {

    ArrayList<Annonce> annonces;
    int page = 0;
    int max;
    @FXML
    private GridPane grid;
    @FXML
    private Button retour;
    @FXML
    private Button suivant;
    @FXML
    private PieChart pichart;
    @FXML
    private PieChart piechart2;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            ArrayList<String> pays = new ArrayList<String>();
            AnnonceService serv = new AnnonceService();
            annonces = serv.getTousAnnonceLibre();
            addPieChartData(piechart2, "masculin", annonces.stream().filter(a -> a.getSexe().equals("masculin")).count());
            addPieChartData(piechart2, "feminin", annonces.stream().filter(a -> a.getSexe().equals("feminin")).count());
            addPieChartData(piechart2, "n'importe", annonces.stream().filter(a -> a.getSexe().equals("both")).count());

            for (int i = 0; i < annonces.size(); i++) {
                final String p = annonces.get(i).getPays();
                if (!pays.contains(p)) {
                    pays.add(p);
                    addPieChartData(pichart, annonces.get(i).getPays(), annonces.stream().filter(a -> a.getPays().equalsIgnoreCase(p)).count());
                }

            }
            max = annonces.size();
            updateGrid(annonces);

        } catch (SQLException ex) {
            Logger.getLogger(AdminAnnonceController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AdminAnnonceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void next(ActionEvent event) throws FileNotFoundException {
        if ((page + 1) * 6 < max) {
            page += 1;
            updateGrid(annonces);
        }
    }

    @FXML
    private void previous(ActionEvent event) throws FileNotFoundException {
        if (page > 0) {
            page -= 1;
            updateGrid(annonces);
        }
    }

    private void updateGrid(ArrayList<Annonce> annonces) throws FileNotFoundException {
        try {
            UserService userservice = new UserService();
            AnnonceService serv = new AnnonceService();
            grid.getChildren().clear();
            for (int i = 0 + 6 * page; i < annonces.size() && i < 6 + 6 * page; i++) {
                final int id = annonces.get(i).getId();
                final int ii = i;
                FontAwesomeIcon times = new FontAwesomeIcon();
                times.setIconName("TIMES");
                times.setSize("35");
                times.setFill(Color.RED);
                times.setOnMouseEntered(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent arg0) {
                        times.setFill(Color.ORANGE);
                    }
                });
                times.setOnMouseExited(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent arg0) {
                        times.setFill(Color.RED);
                    }
                });
                times.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent arg0) {
                        try {
                            serv.supprimerAnnonceParid(annonces.get(ii).getId());
                            annonces.remove(ii);
                            updateGrid(annonces);
                        } catch (SQLException ex) {
                            Logger.getLogger(MesAnnoncesController.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(MesAnnoncesController.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                });
                final Label f = new Label(userservice.findUserById(annonces.get(i).getId_user()).getUsername());
                f.setStyle("-fx-text-fill:#100BA4;-fx-font-size: 13pt;");
                String photourl = userservice.findUserById(annonces.get(i).getId_user()).getPhoto_membre();
                Image image;
                if (photourl == null || photourl == "") {
                    image = new Image(new URL("http://localhost:/pi/images/default.jpg").openStream());
                } else {
                    image = new Image(new URL("http://localhost:/pi/images/" + photourl).openStream());
                }
                ImageView imageView = new ImageView(image);
                imageView.setFitHeight(80);
                imageView.setFitWidth(100);
                GridPane.setConstraints(imageView, 0, (i - 6 * page));
                grid.getChildren().add(imageView);
                GridPane.setConstraints(f, 1, (i - 6 * page));
                grid.getChildren().add(f);
                GridPane.setConstraints(times, 2, (i - 6 * page));
                grid.getChildren().add(times);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RechercherAnnonceController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RechercherAnnonceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addPieChartData(PieChart pChart, String name, long value) {
        final Data data = new Data(name, value);
        pChart.getData().add(data);
    }

}
