/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mnd.entities.Annonce;
import mnd.entities.Reservation_annonce;
import mnd.entities.User;
import mnd.entities.Vote_annonce;
import mnd.services.AnnonceService;
import mnd.services.Reservation_annonceService;
import mnd.services.UserService;
import mnd.services.Vote_annonceService;
import org.controlsfx.control.Rating;

/**
 * FXML Controller class
 *
 * @author kaisf
 */
public class ReservationController implements Initializable {

    User u = AuthentificationController.getInstance().getUser();
    int id_annonce = RechercherAnnonceController.getInstance().getid();
    Annonce annonce;
    @FXML
    private Label description;
    @FXML
    private Label age;
    @FXML
    private Label sexe;
    @FXML
    private Label status;
    @FXML
    private Label error;
    @FXML
    private DatePicker expiration;
    @FXML
    private AnchorPane ap;
    @FXML
    private ImageView imageView;
    @FXML
    private Label username;
    @FXML
    private Button reserver;
    @FXML
    private Button envoyer;
    @FXML
    private JFXDatePicker reservation;
    @FXML
    private Label pays;
    @FXML
    private Label nbpersonne;
    @FXML
    private JFXTextField nb;
    @FXML
    private Rating rating;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            AnnonceService serv = new AnnonceService();
            UserService userserv = new UserService();
            annonce = serv.getAnnonceByid(id_annonce);
            if (u.getId() == annonce.getId_user()) {
                envoyer.setVisible(false);
                reserver.setVisible(false);
            }
            username.setText(userserv.findUserById(annonce.getId_user()).getUsername());
            description.setText(annonce.getDescription());
            age.setText("age minimal : " + annonce.getAge() + "+");
            pays.setText("pays : " + annonce.getPays());
            nbpersonne.setText("personnes:(max : " + annonce.getNb_personne() + "):");
            if (annonce.getSexe().equals("both")) {
                sexe.setText("genre : n'importe");
            } else {
                sexe.setText("genre : " + annonce.getSexe());
            }
            if (annonce.getStatus().equals("l")) {
                status.setText("libre jusqu'à " + annonce.getDate_expiration());
            } else {
                status.setText("libre depuis ...");
            }
            Image image;
            if (annonce.getImage_url() != "") {
                image = new Image(new URL("http://localhost/pi/images/" + annonce.getImage_url()).openStream());
            } else {
                image = new Image(new URL("http://localhost/pi/images/unkown-home.jpg").openStream());
            }

            imageView.setImage(image);
            Vote_annonceService v = new Vote_annonceService();
            ArrayList<Vote_annonce> votes = v.getVotesByIdUser(annonce.getId_user());
            if (votes.size() == 0) {
                rating.setRating(2.5);
            } else {
                int vote = 0;
                for (int k = 0; k < votes.size(); k++) {
                    vote += votes.get(k).getVote();
                }
                rating.setRating((double) vote / (double) votes.size());

            }
            username.setStyle("-fx-text-fill:#100BA4;-fx-font-size: 13pt;");
            username.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent arg0) {
                    username.setStyle("-fx-text-fill:#5381A4;-fx-underline: true;-fx-font-size: 13pt;");
                }
            });
            username.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent arg0) {
                    username.setStyle("-fx-text-fill:#100BA4;-fx-underline: false;-fx-font-size: 13pt;");
                }
            });
            username.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent arg0) {
                    try {
                        UserService userserv = new UserService();
                        FXMLLoader fxmlLoader2 = new FXMLLoader(getClass().getResource("/mnd/gui/Acueil.fxml"));
                        fxmlLoader2.load();
                        AcueilController controller2 = fxmlLoader2.<AcueilController>getController();
                        controller2.setUser(userserv.findUserByUsername(username.getText()));
                        Parent root = FXMLLoader.load(getClass().getResource("/mnd/gui/Profile.fxml"));
                        Stage stage = (Stage) ap.getScene().getWindow();
                        stage.close();
                        stage = new Stage();
                        Scene scene = new Scene(root);
                        scene.setFill(Color.TRANSPARENT);
                        stage.setScene(scene);
                        stage.initStyle(StageStyle.TRANSPARENT);
                        stage.show();
                    } catch (IOException ex) {
                        Logger.getLogger(RechercherAnnonceController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (SQLException ex) {
                        Logger.getLogger(ReservationController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            });
        } catch (SQLException ex) {
            Logger.getLogger(ReservationController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReservationController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(ReservationController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReservationController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void reserver(ActionEvent event) throws SQLException {
        Reservation_annonceService serv = new Reservation_annonceService();
        Date datenow = new Date(System.currentTimeMillis());
        if (u.getId() == annonce.getId_user()) {
            error.setText("Impossible de reserver votre annonce");
        } else if (nb.getText().equals("")) {
            error.setText("entrer nombre de personnes");
        } else if (Integer.parseInt(nb.getText()) > annonce.getNb_personne()) {
            error.setText("nombre de personne inacceptable");
        } else if (expiration.getValue() == null) {
            error.setText("entrer une date de fin");
        } else if (java.sql.Date.valueOf(expiration.getValue()).getTime() < datenow.getTime()) {
            error.setText("date fin expiré");
        } else if (reservation.getValue() == null) {
            error.setText("entrer une date de debut ");
        } else if (java.sql.Date.valueOf(reservation.getValue()).getTime() < datenow.getTime()) {
            error.setText("date debut expiré");
        } else if (java.sql.Date.valueOf(reservation.getValue()).getTime() > java.sql.Date.valueOf(annonce.getDate_expiration()).getTime()) {
            error.setText("date de debut inacceptable");
        } else if (java.sql.Date.valueOf(expiration.getValue()).getTime() > java.sql.Date.valueOf(annonce.getDate_expiration()).getTime()) {
            error.setText("date de fin inacceptable");
        } else if (java.sql.Date.valueOf(expiration.getValue()).getTime() < java.sql.Date.valueOf(reservation.getValue()).getTime()) {
            error.setText("date debut > date fin ?");
        } else {
            Reservation_annonce reservation_annonce = new Reservation_annonce(0, u.getId(), id_annonce, reservation.getValue().toString(), expiration.getValue().toString());
            serv.ajouterReservation(reservation_annonce);
            Stage primarystage = (Stage) ap.getScene().getWindow();
            primarystage.close();
        }
    }

    @FXML
    private void mail(ActionEvent event) throws SQLException, IOException {
        Parent root = null;
        root = FXMLLoader.load(getClass().getResource("/mnd/gui/AnnonceMail.fxml"));
        Stage stage2 = new Stage();
        Scene newScene = new Scene(root);
        stage2.setScene(newScene);
        stage2.show();
    }

}
