/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import mnd.controllers.AuthentificationController;
import mnd.entities.Endroit;
import mnd.entities.User;
import mnd.services.EndroitServices;

/**
 * FXML Controller class
 *
 * @author ahmed
 */
public class EndroitRootController implements Initializable {


    @FXML
    private BorderPane borderPane;
    @FXML
    private GridPane gridPane;
    @FXML
    private VBox vbox;
    @FXML
    private VBox vbox2;
    @FXML
    private VBox vbox1;
    @FXML
    private VBox vbox3;

    EndroitServices es ;
    User user = AuthentificationController.getInstance().getUser();

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
            try {
                try {
                    es = new EndroitServices();
                    vbox1.setOnMouseEntered(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent arg0) {
                            vbox1.setStyle("-fx-background-color: #ffd018");
                        }
                    });
                    vbox1.setOnMouseExited(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent arg0) {
                            vbox1.setStyle("-fx-background-color: #ffae19");
                        }
                    });
                    
                    vbox2.setOnMouseEntered(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent arg0) {
                            vbox2.setStyle("-fx-background-color: #ffd018");
                        }
                    });
                    vbox2.setOnMouseExited(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent arg0) {
                            vbox2.setStyle("-fx-background-color: #ffae19");
                        }
                    });
                    vbox3.setOnMouseEntered(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent arg0) {
                            vbox3.setStyle("-fx-background-color: #ffd018");
                        }
                    });
                    vbox3.setOnMouseExited(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent arg0) {
                            vbox3.setStyle("-fx-background-color: #ffae19");
                        }
                    });
                } catch (SQLException ex) {
                    Logger.getLogger(EndroitRootController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            Parent root = null;
            root = FXMLLoader.load(getClass().getResource("/mnd/gui/Endroit.fxml"));
            borderPane.setCenter(root);
//            Parent root = null;
///           root = FXMLLoader.load(getClass().getResource("/mnd/gui/Endroit.fxml"));
//            borderPane.setCenter(root);
            } catch (IOException ex) {
                Logger.getLogger(EndroitRootController.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

    @FXML
    private void listeEndroits(MouseEvent event) throws IOException {
        Parent root = null;
        root = FXMLLoader.load(getClass().getResource("/mnd/gui/Endroit.fxml"));
        borderPane.setCenter(root);
    }

    @FXML
    private void mesEndroits(MouseEvent event) throws IOException, SQLException {
        Parent root = null;
        root= FXMLLoader.load(getClass().getResource("/mnd/gui/UserEndroits.fxml"));
        borderPane.setCenter(root);

    }

    @FXML
    private void ajouterEndroit(MouseEvent event) throws IOException {
        Parent root = null;
        root = FXMLLoader.load(getClass().getResource("/mnd/gui/AjoutEndroit.fxml"));
        borderPane.setCenter(root);
    }

}