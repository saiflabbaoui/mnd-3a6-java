/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import mnd.controllers.AcueilController;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mnd.controllers.AuthentificationController;
import mnd.entities.Status;
import mnd.entities.User;
import mnd.entities.Vote_annonce;
import mnd.services.StatusService;
import mnd.services.UserService;
import mnd.services.Vote_annonceService;
import org.controlsfx.control.Rating;

/**
 * FXML Controller class
 *
 * @author Camilia
 */
public class ProfileController implements Initializable {

    User cu = AuthentificationController.getInstance().getUser();
    User u = AcueilController.getInstance().getUser();
    UserService us;
    StatusService ss;
    Status s;

    @FXML
    private Label nomL;
    @FXML
    private Label numL;
    @FXML
    private Label emailL;
    @FXML
    private Label paysL;
    @FXML
    private Label ageL;
    @FXML
    private ImageView imgL;
    @FXML
    private HBox coverL;
    @FXML
    private VBox vv;

    @FXML
    private Button ac;
    @FXML
    private Button edit;

    @FXML
    private JFXTextArea statusL;
    @FXML
    private Button ajoutB;

    private ArrayList<Status> status;
    @FXML
    private FontAwesomeIcon log;
    @FXML
    private FontAwesomeIcon logout;
    @FXML
    private JFXTextField recherchet;
    @FXML
    private Button rechercheb;
    @FXML
    private Rating rating;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (u.getUsername().equals(cu.getUsername())) {
            try {
                Vote_annonceService v = new Vote_annonceService();
                ArrayList<Vote_annonce> votes = v.getVotesByIdUser(u.getId());
                if (votes.size() == 0) {
                    rating.setRating(2.5);
                } else {
                    int vote = 0;
                    for (int k = 0; k < votes.size(); k++) {
                        vote += votes.get(k).getVote();
                    }
                    rating.setRating((double) vote / (double) votes.size());

                }
                rating.setDisable(true);
                edit.setVisible(true);
                ajoutB.setVisible(true);
                statusL.setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(ProfileController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                Vote_annonceService serv = new Vote_annonceService();
                ArrayList<Vote_annonce> votes = serv.getVotesByIdUser(u.getId());
                rating.setRating(3);
                for (int i = 0; i < votes.size(); i++) {
                    if (votes.get(i).getId_user() == u.getId() && votes.get(i).getId_voteur() == cu.getId()) {
                        rating.setRating(votes.get(i).getVote());
                    }
                }
                edit.setVisible(false);
                ajoutB.setVisible(false);
                statusL.setVisible(false);
            } catch (SQLException ex) {
                Logger.getLogger(ProfileController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        nomL.setText(u.getUsername());
        numL.setText(Integer.toString(u.getPhone_number()));
        emailL.setText(u.getEmail());
        paysL.setText(u.getCountry());
        ageL.setText(Integer.toString(u.getAge()));
        File file = new File(u.getPhoto_membre());
        Image image = new Image(file.toURI().toString());
        imgL.setImage(image);
        imgL.setFitWidth(50);
        imgL.setFitHeight(50);
        coverL.setStyle("-fx-background-image: url('/mnd/images/" + u.getCountry() + ".jpg')");
        try {
            us = new UserService();
            ss = new StatusService();
        } catch (SQLException ex) {
            Logger.getLogger(ProfileController.class.getName()).log(Level.SEVERE, null, ex);
        }
        int id = u.getId();
        status = (ArrayList<Status>) ss.getUserStatus(id);
        Collections.reverse(status);
        updateStatus(status);

    }

    private void updateStatus(ArrayList<Status> status) {
        vv.getChildren().clear();

        s = new Status();
        int id = u.getId();

        String lb_contenu;
        try {
            us = new UserService();
            ss = new StatusService();
        } catch (SQLException ex) {
            Logger.getLogger(ProfileController.class.getName()).log(Level.SEVERE, null, ex);
        }
//      ObservableList<Status> sl = FXCollections.observableArrayList(ss.getUserStatus(id));
        List st = ss.getUserStatus(id);

        for (int i = 0; i < status.size(); i++) {
            HBox hb = new HBox();

            lb_contenu = status.get(i).getContenu();
            JFXTextArea lb = new JFXTextArea(lb_contenu);
//            lb = new Label(lb_contenu);
            lb.setWrapText(true);
            lb.setPrefSize(400, 100);
//            lb.setTextAlignment(TextAlignment.JUSTIFY);
            FontAwesomeIcon icon = new FontAwesomeIcon();
            icon.setIcon(de.jensd.fx.glyphs.fontawesome.FontAwesomeIconName.EYEDROPPER);
            icon.setFill(Color.ORANGE);
            icon.setStyle("-fx-font-family:\"FontAwesome\";-fx-font-size:20px");
            FontAwesomeIcon icon2 = new FontAwesomeIcon();
            icon2.setIcon(de.jensd.fx.glyphs.fontawesome.FontAwesomeIconName.TRASH);
            icon2.setFill(Color.ORANGE);
            icon2.setStyle("-fx-font-family:\"FontAwesome\";-fx-font-size:20px");
            if (u.getUsername().equals(cu.getUsername())) {
                hb.getChildren().addAll(lb, icon, icon2);
            } else {
                hb.getChildren().addAll(lb);
            }

            hb.setStyle("-fx-spacing:20");
            vv.getChildren().addAll(hb);
            vv.setMinWidth(800);

            lb.setId("lb_profile_style");
            vv.setId("vbox_style");
            s = status.get(i);
            icon.setId(Integer.toString(status.get(i).getId()));
            icon2.setId(Integer.toString(status.get(i).getId()));

            icon.setOnMouseClicked(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent arg0) {
                    int id_user = s.getId_user();
                    s = new Status(id_user, lb.getText());
                    s.setId(Integer.parseInt(icon.getId()));
                    ss.updateStatus(s);
                }
            });
            icon2.setOnMouseClicked(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent arg0) {

                    int sd = Integer.parseInt(icon.getId());
                    s = ss.findStatusById(sd);
                    System.out.println(s);
                    status.remove(s);
                    System.out.println(status);
                    ss.deleteStatus(sd);

                    updateStatus(status);
                    System.out.println(status);
                }
            });

        }
    }

    @FXML
    private void acueilBtn(ActionEvent event) throws IOException {
        Stage stage = (Stage) ac.getScene().getWindow();

        Parent root = FXMLLoader.load(getClass().getResource("/mnd/gui/Ui.fxml"));
        stage.close();
        stage = new Stage();
        Scene scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);
        stage.setScene(scene);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.show();
    }

    @FXML
    private void quit(MouseEvent event) {
        Stage stage = (Stage) logout.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void editBtn(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/mnd/gui/ModifierProfile.fxml"));

        Stage stage = new Stage();
        Scene scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);
        stage.setScene(scene);
        stage.initStyle(StageStyle.TRANSPARENT);

        stage.show();
    }

    @FXML
    private void ajoutS(ActionEvent event) throws SQLException {
        String contenu = statusL.getText();
        int id_user = u.getId();
        Status s = new Status(id_user, "24/02/2019", contenu);
        ss = new StatusService();
        int id = ss.addStatus(s);
        s.setId(id);
        System.out.println(s);
        status.add(0, s);
        updateStatus(status);
    }

    @FXML
    private void disconnect(MouseEvent event) throws IOException {
        Stage stage = (Stage) log.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getResource("/mnd/gui/Authentification.fxml"));
        stage.close();
        stage = new Stage();
        Scene scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);
        stage.setScene(scene);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.show();
    }

    @FXML
    private void recherchec(ActionEvent event) {
        List<Status> sl_filtrer = status;
        System.out.println(sl_filtrer);
        sl_filtrer = sl_filtrer.stream().filter(e -> e.getContenu().contains(recherchet.getText())).collect(Collectors.toList());
        updateStatus((ArrayList<Status>) sl_filtrer);
    }

    @FXML
    private void updatevote(MouseEvent event) throws SQLException {
        Vote_annonceService serv = new Vote_annonceService();
        Vote_annonce v = new Vote_annonce(1, u.getId(), cu.getId(), (int) rating.getRating());
        System.out.println(v);
        ArrayList<Vote_annonce> votes = serv.getVotesByIdUser(u.getId());
        int k = 0;
        for (int i = 0; i < votes.size(); i++) {
            if (votes.get(i).getId_user() == u.getId() && votes.get(i).getId_voteur() == cu.getId()) {
                k = 1;
                Vote_annonce vv = votes.get(i);
                vv.setVote((int) rating.getRating());
                serv.ModifierVote_annonce(vv);
            }
        }
        if (k == 0) {
            serv.ajouterVote_annonce(v);
        }
    }

}
