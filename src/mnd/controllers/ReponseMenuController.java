/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;
import mnd.controllers.AuthentificationController;
import mnd.entities.Question;
import mnd.entities.Reponse;
import mnd.entities.User;
import static mnd.controllers.QuestionMenuController.question_id;
import mnd.services.QuestionService;
import mnd.services.ReponseService;
import mnd.services.UserService;
import org.controlsfx.control.Notifications;

/**
 * FXML Controller class
 *
 * @author Moataz
 */
public class ReponseMenuController implements Initializable {

    public static int reponse_id;
    int reponseSelected;
    private TextArea repondreArea;
    @FXML
    private Button repondre;

    @FXML
    private TableView<Reponse> reponseTable;
    @FXML
    private TableColumn<Reponse, String> username;
    @FXML
    private TableColumn<Reponse, String> date;
    @FXML
    private TableColumn<Reponse,String> votes;
    @FXML
    private TableColumn<Reponse, String> reponses;

    private ObservableList<Reponse> list;
    @FXML
    private Label contenu;
    @FXML
    private JFXTextField repondreField;
    public static Stage s = new Stage();
    User nu;
    User u = AuthentificationController.getInstance().getUser();
    UserService us;
    @FXML
    private JFXButton modifier;
    @FXML
    private FontAwesomeIcon voteup;
    @FXML
    private FontAwesomeIcon devote;
    @FXML
    private JFXButton supprimer;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        refresh();

    }

    @FXML
    private void repondreAction(ActionEvent event) {
        ReponseService rs = new ReponseService();
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        String contenu = repondreField.getText();
        if (contenu.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez saisir une reponse");
            alert.showAndWait();
        } else {
           
            Reponse r = new Reponse(question_id, u.getId(), contenu, 1, timeStamp);
            rs.addReponse(r);
            Notifications notificationBuilder = Notifications.create()
                    .title("Question")
                    .text("votre reponse a eté ajouté")
                    .graphic(null)
                    .hideAfter(Duration.seconds(5))
                    .position(Pos.TOP_RIGHT);
            notificationBuilder.darkStyle();
            notificationBuilder.showInformation();
            refresh();
        }
    }

    public void refresh() {
        try {
            QuestionService qs = new QuestionService();
            Question q = qs.afficherMaQuestion(question_id);
            contenu.setText(q.getContenu());
            ReponseService rs = new ReponseService();
            UserService t = new UserService();

            list = FXCollections.observableArrayList(rs.afficherReponse(question_id));
          
            reponses.setCellValueFactory(new PropertyValueFactory<>("contenu"));
            date.setCellValueFactory(new PropertyValueFactory<>("date_pub"));
            votes.setCellValueFactory(i -> {
                int b = rs.AfficherNbVote(i.getValue().getId()).size();
                SimpleStringProperty aa = new SimpleStringProperty(Integer.toString(b));
                return aa;
            });
            username.setCellValueFactory(i -> {
                SimpleStringProperty a = new SimpleStringProperty(t.findUserById(i.getValue().getId_user()).getUsername());
                return a;
            });
            reponseTable.setItems(list);
        } catch (SQLException ex) {
            Logger.getLogger(ReponseMenuController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void modifierAction(ActionEvent event) throws IOException {
        ReponseService rs = new ReponseService();
        if (u.getId() == rs.afficherMaReponse(reponse_id).getId_user()) {
            Parent root = FXMLLoader.load(getClass().getResource("/mnd/gui/ModifierReponse.fxml"));

            Scene scene = new Scene(root);
            s.setScene(scene);
            s.show();
        } else {
            Notifications notificationBuilder = Notifications.create()
                    .title("Reponse")
                    .text("c'est pas votre reponse !")
                    .graphic(null)
                    .hideAfter(Duration.seconds(5))
                    .position(Pos.TOP_RIGHT);
            notificationBuilder.darkStyle();
            notificationBuilder.showError();
            
        }
         refresh();
        
    }

    @FXML
    private void getReponse(MouseEvent event) {
        reponseSelected = reponseTable.getSelectionModel().getSelectedItem().getId();
        reponse_id = reponseSelected;
        ReponseService rs = new ReponseService();
        int hasVoted = rs.voteExist(u.getId(), reponse_id);
        if (hasVoted == 0) {
            voteup.setDisable(false);
            devote.setDisable(true);
        } else {
            Notifications notificationBuilder = Notifications.create()
                    .title("Vote")
                    .text("Tu as deja voté à cette reponse!")
                    .graphic(null)
                    .hideAfter(Duration.seconds(5))
                    .position(Pos.TOP_RIGHT);
            notificationBuilder.darkStyle();
            notificationBuilder.showError();
            voteup.setDisable(true);
            devote.setDisable(false);

        }

    }

    @FXML
    private void vote(MouseEvent event) {
        ReponseService rs = new ReponseService();
        rs.voteReponse(u.getId(), reponse_id);
        refresh();

    }

    @FXML
    private void devote(MouseEvent event) {
        ReponseService rs = new ReponseService();
        rs.unvoteReponse(u.getId(), reponse_id);
        refresh();
    }

    @FXML
    private void supprimerAction(ActionEvent event) {
        ReponseService rs = new ReponseService();
        if (u.getId() == rs.afficherMaReponse(reponse_id).getId_user()) {
            Reponse reponse = rs.afficherMaReponse(reponseSelected);
            rs.supprimerReponse(reponse);
            Notifications notificationBuilder = Notifications.create()
                    .title("Reponse")
                    .text("votre reponse a eté supprimé")
                    .graphic(null)
                    .hideAfter(Duration.seconds(5))
                    .position(Pos.TOP_RIGHT);
            notificationBuilder.darkStyle();
            notificationBuilder.showWarning();
        } else {
            Notifications notificationBuilder = Notifications.create()
                    .title("Reponse")
                    .text("c'est pas votre reponse !")
                    .graphic(null)
                    .hideAfter(Duration.seconds(5))
                    .position(Pos.TOP_RIGHT);
            notificationBuilder.darkStyle();
            notificationBuilder.showError();

        }
        refresh();
    }
}
