/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import com.jfoenix.controls.JFXComboBox;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.activation.MimetypesFileTypeMap;
import mnd.entities.Annonce;
import mnd.entities.User;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import mnd.services.AnnonceService;

/**
 *
 * @author kaisf
 */
public class AjouterController implements Initializable {

    private final ObservableList<String> listePays = FXCollections.observableArrayList("", "Afghanistan", " Albanie", " Antarctique", " Algérie", " Samoa Américaines", " Andorre", " Angola", " Antigua-et-Barbuda", " Azerbaïdjan", " Argentine", " Australie", " Autriche", " Bahamas", " Bahreïn", " Bangladesh", " Arménie", " Barbade", " Belgique", " Bermudes", " Bhoutan", " Bolivie", " Bosnie-Herzégovine", " Botswana", " Île Bouvet", " Brésil", " Belize", " Territoire Britannique de Océan Indien", " Îles Salomon", " Îles Vierges Britanniques", " Brunéi Darussalam", " Bulgarie", " Myanmar", " Burundi", " Bélarus", " Cambodge", " Cameroun", " Canada", " Cap-vert", " Îles Caïmanes", " République Centrafricaine", " Sri Lanka", " Tchad", " Chili", " Chine", " Taïwan", " Île Christmas", " Îles Cocos (Keeling)", " Colombie", " Comores", " Mayotte", " République du Congo", " République Démocratique du Congo", " Îles Cook", " Costa Rica", " Croatie", " Cuba", " Chypre", " République Tchèque", " Bénin", " Danemark", " Dominique", " République Dominicaine", " Équateur", " El Salvador", " Guinée Équatoriale", " Éthiopie", " Érythrée", " Estonie", " Îles Féroé", " Îles (malvinas) Falkland", " Géorgie du Sud et les Îles Sandwich du Sud", " Fidji", " Finlande", " Îles Åland", " France", " Guyane Française", " Polynésie Française", " Terres Australes Françaises", " Djibouti", " Gabon", " Géorgie", " Gambie", " Territoire Palestinien Occupé", " Allemagne", " Ghana", " Gibraltar", " Kiribati", " Grèce", " Groenland", " Grenade", " Guadeloupe", " Guam", " Guatemala", " Guinée", " Guyana", " Haïti", " Îles Heard et Mcdonald", " Saint-Siège (état de la Cité du Vatican)", " Honduras", " Hong-Kong", " Hongrie", " Islande", " Inde", " Indonésie", " République Islamique Iran", " Iraq", " Irlande", " Israël", " Italie", " Côte Ivoire", " Jamaïque", " Japon", " Kazakhstan", " Jordanie", " Kenya", " République Populaire Démocratique de Corée", " République de Corée", " Koweït", " Kirghizistan", " République Démocratique Populaire Lao", " Liban", " Lesotho", " Lettonie", " Libéria", " Jamahiriya Arabe Libyenne", " Liechtenstein", " Lituanie", " Luxembourg", " Macao", " Madagascar", " Malawi", " Malaisie", " Maldives", " Mali", " Malte", " Martinique", " Mauritanie", " Maurice", " Mexique", " Monaco", " Mongolie", " République de Moldova", " Montserrat", " Maroc", " Mozambique", " Oman", " Namibie", " Nauru", " Népal", " Pays-Bas", " Antilles Néerlandaises", " Aruba", " Nouvelle-Calédonie", " Vanuatu", " Nouvelle-Zélande", " Nicaragua", " Niger", " Nigéria", " Niué", " Île Norfolk", " Norvège", " Îles Mariannes du Nord", " Îles Mineures Éloignées des États-Unis", " États Fédérés de Micronésie", " Îles Marshall", " Palaos", " Pakistan", " Panama", " Papouasie-Nouvelle-Guinée", " Paraguay", " Pérou", " Philippines", " Pitcairn", " Pologne", " Portugal", " Guinée-Bissau", " Timor-Leste", " Porto Rico", " Qatar", " Réunion", " Roumanie", " Fédération de Russie", " Rwanda", " Sainte-Hélène", " Saint-Kitts-et-Nevis", " Anguilla", " Sainte-Lucie", " Saint-Pierre-et-Miquelon", " Saint-Vincent-et-les Grenadines", " Saint-Marin", " Sao Tomé-et-Principe", " Arabie Saoudite", " Sénégal", " Seychelles", " Sierra Leone", " Singapour", " Slovaquie", " Viet Nam", " Slovénie", " Somalie", " Afrique du Sud", " Zimbabwe", " Espagne", " Sahara Occidental", " Soudan", " Suriname", " Svalbard etÎle Jan Mayen", " Swaziland", " Suède", " Suisse", " République Arabe Syrienne", " Tadjikistan", " Thaïlande", " Togo", " Tokelau", " Tonga", " Trinité-et-Tobago", " Émirats Arabes Unis", " Tunisie", " Turquie", " Turkménistan", " Îles Turks et Caïques", " Tuvalu", " Ouganda", " Ukraine", "ex-République Yougoslave de Macédoine", " Égypte", " Royaume-Uni", " Île de Man", " République-Unie de Tanzanie", " États-Unis", " Îles Vierges des États-Unis", " Burkina Faso", " Uruguay", " Ouzbékistan", " Venezuela", " Wallis et Futuna", " Samoa", " Yémen", " Serbie-et-Monténégro", " Zambie");
    User u = AuthentificationController.getInstance().getUser();
    String imageUrl = "";
    @FXML
    private Label error;
    @FXML
    private TextArea description;
    @FXML
    private JFXComboBox<String> pays;
    @FXML
    private DatePicker expiration;
    @FXML
    private TextField age;
    @FXML
    private TextField personne;
    @FXML
    private RadioButton masculin;
    @FXML
    private RadioButton feminin;
    @FXML
    private AnchorPane ap;
    @FXML
    private Label notif;
    @FXML
    private Button confirmer;
    @FXML
    private Button ajouter;
    @FXML
    private ImageView imageview;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        pays.setItems(listePays);
    }

    //setting the upload action for our photo
    @FXML
    private void upload(ActionEvent event) throws SQLException, IOException {
        ImageView imgView = new ImageView();//setting imageview where we will set our uploaded picture before taking it to the database
        FileChooser fc = new FileChooser();
        Stage primarystage = (Stage) ap.getScene().getWindow();
        File source = fc.showOpenDialog(primarystage);
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpPost httppost = new HttpPost("http://localhost/pi/upload.php");
            File newFile = new File(source.getParent(), Integer.toString(u.getId()) + source.getName());
            Files.copy(source.toPath(), newFile.toPath());
            FileBody bin = new FileBody(newFile);
            HttpEntity reqEntity = MultipartEntityBuilder.create()
                    .addPart("fileToUpload", bin)
                    .build();

            httppost.setEntity(reqEntity);
            CloseableHttpResponse response = httpclient.execute(httppost);
            try {
                String mimetype = new MimetypesFileTypeMap().getContentType(source);
                String type = mimetype.split("/")[0];
                imageUrl = newFile.getName();
                notif.setText("image téléchargé");
                Image image = new Image(new URL("http://localhost:/pi/images/" + imageUrl).openStream());
                imageview.setImage(image);

                HttpEntity resEntity = response.getEntity();

                EntityUtils.consume(resEntity);
                newFile.delete();

            } finally {
                response.close();
            }
        } finally {
            httpclient.close();
        }

    }

    @FXML
    private void handleButtonAction(ActionEvent event) throws SQLException, IOException {
        AnnonceService serv = new AnnonceService();
        System.out.println(serv.getAnnonceParuserid(u.getId()));
        if (serv.getAnnonceParuserid(u.getId()).size() != 0) {
            error.setText("Vous avez déja une annonce inscrite");
        } else if (pays.getValue() == null || pays.getValue().equals("")) {
            error.setText("entrer un pays");
        } else if (age.getText() == null || age.getText().isEmpty()) {
            error.setText("entrer votre age");
        } else if (personne.getText() == null || personne.getText().isEmpty()) {
            error.setText("entrer nombre de personnes");
        } else if (!isInteger(age.getText(), 10)) {
            error.setText("age invalide");
        } else if (Integer.parseInt(age.getText()) < 12) {
            error.setText("désolé interdit au moins de 12 ans");
        } else if (!isInteger(personne.getText(), 10)) {
            error.setText("nomre de personne invalide");
        } else if (Integer.parseInt(personne.getText()) <= 0) {
            error.setText("nombre de personne doit être positive");
        } else if (expiration.getValue() == null) {
            error.setText("entrer une date d'expiration");
        } else if (masculin.selectedProperty().get() == false && feminin.selectedProperty().get() == false) {
            error.setText("cocher un sexe");
        } else {
            String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
            Date datenow = new Date(System.currentTimeMillis());
            Date expirationDate = java.sql.Date.valueOf(expiration.getValue());
            if (expirationDate.getTime() < datenow.getTime()) {
                error.setText("date expiré");
            } else {
                error.setText("");
                String sexe;
                if (masculin.selectedProperty().get() == true && feminin.selectedProperty().get() == true) {
                    sexe = "both";
                } else if (masculin.selectedProperty().get() == true) {
                    sexe = "masculin";
                } else {
                    sexe = "feminin";
                }
                if (description.getText() == null) {
                    description.setText("");
                }
                String dateannonce = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
                Annonce annonce = new Annonce(1, u.getId(), dateannonce, description.getText(), Integer.parseInt(personne.getText()), sexe, Integer.parseInt(age.getText()), expiration.getValue().toString(), imageUrl, "l", (String) pays.getValue());
                serv.ajouterAnnonce(annonce);
                Parent root2 = FXMLLoader.load(getClass().getResource("/mnd/gui/annonces.fxml"));
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/mnd/gui/Ui.fxml"));
                Parent root = (Parent) fxmlLoader.load();
                UiController controller = fxmlLoader.<UiController>getController();
                controller.setParent(root2);
                Scene scene = new Scene(root);
                Stage stage = (Stage) ap.getScene().getWindow();
                stage.setScene(scene);
                stage.show();

            }
        }
    }

    public static boolean isInteger(String s, int radix) {
        if (s.isEmpty()) {
            return false;
        }
        for (int i = 0; i < s.length(); i++) {
            if (i == 0 && s.charAt(i) == '-') {
                if (s.length() == 1) {
                    return false;
                } else {
                    continue;
                }
            }
            if (Character.digit(s.charAt(i), radix) < 0) {
                return false;
            }
        }
        return true;
    }
}
