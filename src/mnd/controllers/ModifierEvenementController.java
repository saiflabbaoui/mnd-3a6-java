/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import com.jfoenix.controls.JFXComboBox;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import static mnd.controllers.AjouterEvenementController.isInteger;
import mnd.entities.Evenement;
import mnd.entities.User;
import mnd.services.EvenementServices;
import mnd.services.UserService;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 * FXML Controller class
 *
 * @author Steve
 */
public class ModifierEvenementController implements Initializable {

    private static int id;

    @FXML
    private TextField titre;
    @FXML
    private TextField nbp;
    @FXML
    private JFXComboBox<String> lieux;
    @FXML
    private DatePicker date;
    @FXML
    private TextArea description;
    @FXML
    private ImageView imageevent;
    @FXML
    private Button ajoutevent;

    String imageURL;

    String imageupload;
    private Label profileLabel;
    @FXML
    private HBox upbox;
    @FXML
    private Label titre1;
    ObservableList<String> Pays
            = FXCollections.observableArrayList("Tunisie", "France", "Espagne", "Allemagne", "Turquie", "USA", "Angleterre");
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
                 lieux.setItems(Pays);    
        EvenementServices es = new EvenementServices();
        Evenement ev = es.findById(id);

        titre.setText(ev.getTitre());
        nbp.setText(ev.getNbr_participant() + "");
        lieux.setValue(ev.getPays());
        description.setText(ev.getDescription());
        //profileLabel.setText(AuthentificationController.getInstance().getUser().getUsername());

        // TODO
    }

    @FXML
    private void ajouterev(ActionEvent event) throws SQLException, IOException {
        EvenementServices es = new EvenementServices();
        String titree = titre.getText();
        String lieuxx = lieux.getValue();
        int nbpp;

        if (titre.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Echec de l'ajout");
            alert.setHeaderText(null);
            alert.setContentText("Attention ! Veuillez saisir un titre  ");

            alert.showAndWait();
        }

        else if ((nbp.getText().isEmpty() ) || !isInteger(nbp.getText())){
            
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Exception numerique");
            alert.setHeaderText(null);
            alert.setContentText("Veillez saisir un nombre de participants");
            alert.showAndWait();
            nbpp = Integer.parseInt(nbp.getText());
        } else if (lieux.getValue().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Echec Ajout");
            alert.setHeaderText(null);
            alert.setContentText("Veillez montionner un pays");
            alert.showAndWait();
        }  else if (description.getText().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Echec Ajout");
            alert.setHeaderText(null);
            alert.setContentText("Veillez ajouter une description");
            alert.showAndWait();
     
                    

             
        
            

        }

        Date datee;
        if (date.getValue() != null) {
            datee = java.sql.Date.valueOf(date.getValue());
        } else {
            datee = null;
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Echec de l'ajout");
                alert.setHeaderText(null);
                alert.setContentText("Attention ! Veillez saisir une date!");
                alert.showAndWait();
           
        }
        String descriptionn = description.getText();
        java.util.Date date_util = new java.util.Date();
        java.sql.Date date_sql = new java.sql.Date(date_util.getTime());

        if (titre.getText().length() > 0 && lieux.getValue().length() > 0 && isInteger(nbp.getText())) {

            if (datee.compareTo(date_sql) < 0 ) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Echec de l'ajout");
                alert.setHeaderText(null);
                alert.setContentText("Attention ! Date invalide !");
                alert.showAndWait();
            } else {

        Evenement e = new Evenement();
        e.setTitre(titre.getText());
        e.setNbr_participant(Integer.parseInt(nbp.getText()));
        e.setPays(lieux.getValue());
        e.setDate(Date.valueOf(date.getValue()));
        e.setDescription(description.getText());
        e.setImage_url(imageURL);
        System.out.println(imageevent.getImage());

        es.update(e, id);
         TrayNotification tr = new TrayNotification();
            tr.setAnimationType(AnimationType.POPUP);
            tr.setTitle("M & D");
            tr.setNotificationType(NotificationType.INFORMATION);
            tr.setMessage("Votre évènement a été modifié avec succes");
            tr.showAndDismiss(Duration.seconds(1));
       

        Stage stage = (Stage) description.getScene().getWindow();
                // do what you have to do
                stage.close();
    
        Parent root = null;
        root = FXMLLoader.load(getClass().getResource("/mnd/gui/DetailsEvenement.fxml"));
        UiController.borderPaneS.setCenter(root);
        

        //mnd.main.Mnd.getInstance().getStage().hide();
        //      mnd.main.Mnd.getInstance().ChangeScene(new Scene(FXMLLoader.load(getClass().getResource("/mnd/DetailsEvenement.fxml"))));
        //    mnd.main.Mnd.getInstance().getStage().show();
    }
        }
    }

    @FXML
    private void uploadphoto(MouseEvent event) throws MalformedURLException, IOException {
        FileChooser fc = new FileChooser();
        File selectedFile = fc.showOpenDialog(null);
        if (selectedFile != null) {
            //    getImageUrl = selectedFile.getAbsolutePath();
            // System.out.println(getImageUrl);
            // Image value = new Image(getImageUrl);
            //  img.setImage(value);
            imageupload = selectedFile.toURI().toURL().toString();
            upload(selectedFile);

            Image img = new Image(imageupload);
            //prt.setImageProduit(selectedFile.getName());
            imageevent.setImage(img);

        } else {
            System.out.println("file doesn't exist");
        }
    }

    private String upload(File file) throws FileNotFoundException, IOException {
        BufferedOutputStream stream = null;
        String globalPath = "C:\\wamp64\\www\\image";
        String localPath = "localhost:80/";
        String fileName = file.getName();
        imageURL = file.getName();
        fileName = fileName.replace(" ", "_");
        try {
            Path p = file.toPath();

            byte[] bytes = Files.readAllBytes(p);

            File dir = new File(globalPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            // Create the file on server
            File serverFile = new File(dir.getAbsolutePath() + File.separator + fileName);
            stream = new BufferedOutputStream(new FileOutputStream(serverFile));
            stream.write(bytes);
            stream.close();
            return localPath + "/" + fileName;

        } catch (FileNotFoundException ex) {
            // Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
            return "error1";
        } catch (IOException ex) {
            // Logger.getLogger(ProduitController.class.getName()).log(Level.SEVERE, null, ex);
            return "error2";

        }

    }

    public void redirect(String id) {
        System.out.println("Interface Affiche Details");
        System.out.println(id);
        this.id = Integer.parseInt(id);

    }

    private void retour(ActionEvent event) throws IOException {
        // mnd.main.Mnd.getInstance().ChangeScene(new Scene(FXMLLoader.load(getClass().getResource("/mnd/gui/AfficherEvenement.fxml"))));
    }

    @FXML
    private void close(MouseEvent event) throws IOException {
        Stage stage = (Stage) description.getScene().getWindow();
                // do what you have to do
                stage.hide();
         DetailsEvenementController l = new DetailsEvenementController();
                                    DetailsEvenementController.id=id;
                                    
                Parent root = FXMLLoader.load(getClass().getResource("/mnd/gui/DetailsEvenement.fxml"));
                UiController.borderPaneS.setCenter(root);
            

    }




}
