/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import static jdk.nashorn.internal.objects.NativeDebug.getClass;
import mnd.controllers.AuthentificationController;
import mnd.entities.Endroit;
import mnd.entities.User;
import static mnd.main.Mnd.stage;
import mnd.services.EndroitServices;

/**
 * FXML Controller class
 *
 * @author Ahmed
 */
public class EndroitController implements Initializable {

    @FXML
    private AnchorPane anchorpane;
    @FXML
    private JFXTextField searchField;
    @FXML
    private JFXListView<Endroit> listEndroits;
    private ObservableList<Endroit> data=FXCollections.observableArrayList();
    EndroitServices es  ;
    @FXML
    private JFXButton filtrer;
    @FXML
    private ChoiceBox<String> pays;
    private final ObservableList<String> listePays = FXCollections.observableArrayList("Tous les pays", "Tunisie", "France", "Espagne", "Allemagne", "Turquie", "USA", "Angleterre");
    
    
    static List<Endroit> endroits = new ArrayList<Endroit>();
    
    User u = AuthentificationController.getInstance().getUser();
    int idEndroitSelected ;
    String titreEndroit ;
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            es = new EndroitServices() ;
            endroits = es.listAll();
            refreshList(endroits);
        } catch (SQLException ex) {
            Logger.getLogger(EndroitController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        pays.setItems(listePays);
        

    }
    
    
    public void refreshList(List<Endroit> endroits){
        
        data.setAll(endroits);
        listEndroits.setCellFactory(new Callback<ListView<Endroit>, ListCell<Endroit>>(){
                   @Override
                   public ListCell<Endroit> call(ListView<Endroit> args0) {
                       ListCell<Endroit> cell;
                       cell = new ListCell<Endroit>(){
                           @Override
                           protected void updateItem(Endroit endroit, boolean btl){
                               super.updateItem(endroit,btl);
                               if(endroit != null){
                                   try {
                                       //String url = endroit.getImage_url();
                                       //File file = new File(url);
                                       // Image image = new Image(file.toURI().toString());
                                       Image image = new Image(new URL("http://localhost:/pi/images/" + endroit.getImage_url()).openStream());
                                       //      endroitImage.setImage(image);
                                       ImageView imgview = new ImageView();
                                       imgview.setImage(image);
                                       imgview.setFitHeight(200);
                                       imgview.setFitWidth(250);
                                       setGraphic(imgview);
                                       
                                       setText("Titre : " + endroit.getTitre() +"\n" + "Pays : " + endroit.getPays() + "\n"+ "Type : " + endroit.getType() ) ;
                                       setFont(Font.font("Verdana",30));
                                       setAlignment(Pos.TOP_LEFT);
                                       setStyle("-fx-padding : 5px");
                                       idEndroitSelected = endroit.getId_endroit();
                                   } catch (IOException ex) {
                                       Logger.getLogger(EndroitController.class.getName()).log(Level.SEVERE, null, ex);
                                   }
                               }
                           }
                       };
                       return cell;
                   }
            });
        listEndroits.depthProperty().set(1);
        listEndroits.setItems(data);
        listEndroits.setOnMouseClicked(new EventHandler<MouseEvent>(){
             @Override
             public void handle(MouseEvent event){
                 try {
                    ObservableList<Endroit> e = listEndroits.getSelectionModel().getSelectedItems() ;
                    System.out.println(e);
                    int id = e.get(0).getId_endroit();
                    System.out.println("id est : " + id  );
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/mnd/gui/EndroitSpecifications.fxml"));
                    EndroitSpecificationsController ctrl = loader.getController();
                    ctrl.idEndroit = id ;
                    Parent root;
                    root = (Parent) loader.load();
                    Scene newScene = new Scene(root);
                    Stage newStage = new Stage();
                    newStage.setTitle("Specifications");
                    newStage.setScene(newScene);
                    newStage.show();
                     
                 } catch (IOException ex) {
                     Logger.getLogger(EndroitController.class.getName()).log(Level.SEVERE, null, ex);
                 }
             
             }
         });
    }

    private void ajouterEndroit(ActionEvent event) throws IOException {
       FXMLLoader loader=new FXMLLoader(getClass().getResource("/mnd/gui/AjoutEndroit.fxml"));
       Parent root=loader.load();
       Scene scene = new Scene(root);
       Stage ajout = new Stage();
       ajout.setScene(scene);
       ajout.show();
    

    }

    @FXML
    private void filtrer(ActionEvent event) {
        List<Endroit> list = new ArrayList<>();

        if(!searchField.getText().equals("") || !pays.getValue().equals("") || !pays.getValue().equals("Tous les pays") ){
            if(!searchField.getText().equals("") && pays.getValue().equals("") ){
              // list = es.filterByTitle(searchField.getText());
                list = endroits.stream().filter(x->x.getTitre().equals(searchField.getText())).collect(Collectors.toList());
            }
            if(searchField.getText().equals("") && ( !pays.getValue().equals("") || !pays.getValue().equals("Tous les pays")) ){
              list =  endroits.stream().filter(x->x.getPays().equals(pays.getValue())).collect(Collectors.toList()) ;
                // list = es.getByCountry(pays.getValue());
            }
            if(!searchField.getText().equals("") && ( !pays.getValue().equals("") || !pays.getValue().equals("Tous les pays"))){
                list=  endroits.stream().filter(x->x.getPays().equals(pays.getValue())).filter(y->y.getTitre().equals(searchField.getText())).collect(Collectors.toList()) ;
                
            }
            if( searchField.getText().equals("") && pays.getValue().equals("Tous les pays")){
                list = endroits.stream().collect(Collectors.toList());
            }
            if(!searchField.getText().equals("") && pays.getValue().equals("Tous les pays")){
                list = endroits.stream().filter(x->x.getTitre().equals(searchField.getText())).collect(Collectors.toList());
            }
        }

        refreshList(list);
    }
    
    

//    public static void setEndroits(List<Endroit> endroits) {
//        EndroitController.endroits = endroits;
//    }

    public static void setEndroits(List<Endroit> endroits) {
        EndroitController.endroits = endroits;
    }

  
    
}