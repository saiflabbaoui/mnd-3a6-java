/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import mnd.entities.User;
import mnd.services.AnnonceService;
import mnd.services.UserService;

/**
 * FXML Controller class
 *
 * @author kaisf
 */
public class AnnonceMail implements Initializable {

    int id_annonce = RechercherAnnonceController.getInstance().getid();
    User u = AuthentificationController.getInstance().getUser();
    @FXML
    private TextArea content;
    @FXML
    private TextField subject;
    @FXML
    private Label notif;
    @FXML
    private AnchorPane ap;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void confirmer(ActionEvent event) throws SQLException, FileNotFoundException, IOException {
        AnnonceService serv = new AnnonceService();
        UserService userserv = new UserService();
        String dest = userserv.findUserById(serv.getAnnonceByid(id_annonce).getId_user()).getEmail();
        System.out.println(dest);
        final String username = "nodignitypi@gmail.com";
        final String password = "nodignity123";
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
            Stage primarystage = (Stage) ap.getScene().getWindow();
            primarystage.close();
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("nodignitypi@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(dest));
            message.setSubject(subject.getText());
            message.setText("de la part de :" + u.getUsername() + "\n\n " + content.getText());
            Transport.send(message);
            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

}
