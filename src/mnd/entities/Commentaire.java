/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.entities;

import java.sql.Date;
import java.time.LocalDate;

/**
 *
 * @author Ahmed
 */
public class Commentaire {
    private int id_comm ; 
    private int id_user; 
    private int id_endroit ; 
    private String text ; 
    private String date ; 

    public Commentaire(int id_user,int id_endroit, String text, String date) {
        this.id_user = id_user;
        this.id_endroit = id_endroit ;
        this.text = text;
        this.date =date;
    }
    public Commentaire(int id_user,int id_endroit, String text) {
        this.id_user = id_user;
        this.id_endroit = id_endroit ;
        this.text = text;
        this.date = LocalDate.now().toString();
    }

    
    public int getId_comm() {
        return id_comm;
    }

    public void setId_comm(int id_comm) {
        this.id_comm = id_comm;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_endroit() {
        return id_endroit;
    }

    public void setId_endroit(int id_endroit) {
        this.id_endroit = id_endroit;
    }
    

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Commentaire{" + "id_comm=" + id_comm + ", id_user=" + id_user + ", id_endroit=" + id_endroit + ", text=" + text + ", date=" + date + "\n"+ '}';
    }
    
    
    
}
