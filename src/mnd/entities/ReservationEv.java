/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.entities;

import java.sql.Date;

/**
 *
 * @author Steve
 */
public class ReservationEv {
    private int id;
    private int id_user;
    private int id_evenement;
    private Date dateRes;

    public ReservationEv(int id_user, int id_evenement, Date dateRes) {
        this.id_user = id_user;
        this.id_evenement = id_evenement;
        this.dateRes = dateRes;
    }

    public ReservationEv(int id, int id_user, int id_evenement, Date dateRes) {
        this.id = id;
        this.id_user = id_user;
        this.id_evenement = id_evenement;
        this.dateRes = dateRes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_evenement() {
        return id_evenement;
    }

    public void setId_evenement(int id_evenement) {
        this.id_evenement = id_evenement;
    }

    public Date getDateRes() {
        return dateRes;
    }

    public void setDateRes(Date dateRes) {
        this.dateRes = dateRes;
    }

    @Override
    public String toString() {
        return "ReservationEv{" + "id=" + id + ", id_user=" + id_user + ", id_evenement=" + id_evenement + ", dateRes=" + dateRes + '}';
    }
    
    
    
    
    
}
