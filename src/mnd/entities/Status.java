/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.entities;

import java.util.Objects;

/**
 *
 * @author Camilia
 */
public class Status {

    private int id;
    private int id_user;
    private String date_pub;
    private String contenu;

    public Status() {

    }

    public Status(String contenu) {
        this.contenu = contenu;
    }

    public Status(int id, int id_user, String date_pub, String contenu) {
        this.id = id;
        this.id_user = id_user;
        this.date_pub = date_pub;
        this.contenu = contenu;
    }

    public Status(int id_user, String string, String contenu) {
        this.id_user = id_user;
        this.date_pub = date_pub;
        this.contenu = contenu;
    }

    public Status(int id_user,String contenu) {
        this.id_user = id_user;     
        this.contenu = contenu;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getDate_pub() {
        return date_pub;
    }

    public void setDate_pub(String date_pub) {
        this.date_pub = date_pub;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    @Override
    public String toString() {
        return "Status{" + "id=" + id + ", id_user=" + id_user + ", date_pub=" + date_pub + ", contenu=" + contenu + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 13 * hash + this.id;
        hash = 13 * hash + this.id_user;
        hash = 13 * hash + Objects.hashCode(this.date_pub);
        hash = 13 * hash + Objects.hashCode(this.contenu);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Status other = (Status) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.id_user != other.id_user) {
            return false;
        }
        if (!Objects.equals(this.date_pub, other.date_pub)) {
            return false;
        }
        if (!Objects.equals(this.contenu, other.contenu)) {
            return false;
        }
        return true;
    }
    
    
    

}
