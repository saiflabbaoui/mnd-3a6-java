/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.entities;

import java.util.Date;

/**
 *
 * @author Moataz
 */
public class Reponse {
    private int id ;
    private int id_question ;
    private int  id_user ;
    private String contenu;
    private  int nb_vote;
    private String date_pub;

    public Reponse(int id_question, int id_user, String contenu, int nb_vote, String date_pub) {
        this.id_question = id_question;
        this.id_user = id_user;
        this.contenu = contenu;
        this.nb_vote = nb_vote;
        this.date_pub = date_pub;
    }

    public Reponse(int id, int id_question, int id_user, String contenu, int nb_vote, String date_pub) {
        this.id = id;
        this.id_question = id_question;
        this.id_user = id_user;
        this.contenu = contenu;
        this.nb_vote = nb_vote;
        this.date_pub = date_pub;
    }
    
    public Reponse() {
    }

    public Reponse(int id, String contenu, int i, String timeStamp) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_question() {
        return id_question;
    }

    public void setId_question(int id_question) {
        this.id_question = id_question;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public int getNb_vote() {
        return nb_vote;
    }

    public void setNb_vote(int nb_vote) {
        this.nb_vote = nb_vote;
    }

    public String getDate_pub() {
        return date_pub;
    }

    public void setDate_pub(String date_pub) {
        this.date_pub = date_pub;
    }

    @Override
    public String toString() {
        return "Reponse{" + "id=" + id + ", id_question=" + id_question + ", user=" + id_user + ", contenu=" + contenu + ", nb_vote=" + nb_vote + ", date_pub=" + date_pub +"\n";
    }
    
    
}
