/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.entities;

import java.sql.Date;

/**
 *
 * @author Steve
 */
public class Evenement implements Comparable<Evenement>  {
    private int id;
    private int id_user;
    private String titre;
    private int nbr_participant;
    private Date date;
    private String description;
    private String image_url;
    private String pays;
    private String longitude;
    private String latitude;

    public Evenement(int id, int id_user,String titre, int nbr_participant, Date date, String description, String image_url, String pays, String longitude, String latitude) {
        this.id = id;
        this.id_user = id_user;
        this.titre = titre;
        this.nbr_participant = nbr_participant;
        this.date = date;
        this.description = description;
        this.image_url = image_url;
        this.pays = pays;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Evenement(int id_user,  String titre, int nbr_participant, Date date, String description, String image_url, String pays, String longitude, String latitude) {
        this.id_user = id_user;
        this.titre = titre;
        this.nbr_participant = nbr_participant;
        this.date = date;
        this.description = description;
        this.image_url = image_url;
        this.pays = pays;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Evenement(String pays, String titre, int nbr_participant, Date date, String description, String image_url) {
        this.pays = pays;
        this.titre = titre;
        this.nbr_participant = nbr_participant;
        this.date = date;
        this.description = description;
        this.image_url = image_url;
    }

    public Evenement(int id, String pays, String titre, int nbr_participant, Date date, String description, String image_url) {
        this.id = id;
        this.pays = pays;
        this.titre = titre;
        this.nbr_participant = nbr_participant;
        this.date = date;
        this.description = description;
        this.image_url = image_url;
    }

   
    
    
    

   

    

    public Evenement() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getNbr_participant() {
        return nbr_participant;
    }

    public void setNbr_participant(int nbr_participant) {
        this.nbr_participant = nbr_participant;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "Evenement{" + "id=" + id + ", id_user=" + id_user + ", titre=" + titre + ", nbr_participant=" + nbr_participant + ", date=" + date + ", description=" + description + ", image_url=" + image_url + ", pays=" + pays + ", longitude=" + longitude + ", latitude=" + latitude + '}';
    }

   
  @Override
  public int compareTo(Evenement o) {
    return this.date.compareTo(o.getDate());
  }
    
    
}
