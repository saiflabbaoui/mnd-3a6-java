/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.entities;

/**
 *
 * @author Moataz
 */
public class Vote {
    int id_user ;
    int id_reponse;

    public Vote(int id_user, int id_reponse) {
        this.id_user = id_user;
        this.id_reponse = id_reponse;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_reponse() {
        return id_reponse;
    }

    public void setId_reponse(int id_reponse) {
        this.id_reponse = id_reponse;
    }
    
    
}
