/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.entities;

/**
 *
 * @author kaisf
 */
public class Reservation_annonce {
    private int id;
    private int id_user;
    private int id_annonce;
    private String date_reservation;
    private String date_expiration;

    public Reservation_annonce(int id, int id_user, int id_annonce, String date_reservation, String date_expiration) {
        this.id=id;
        this.id_user=id_user;
        this.id_annonce=id_annonce;
        this.date_reservation=date_reservation;
        this.date_expiration=date_expiration;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the id_user
     */
    public int getId_user() {
        return id_user;
    }

    /**
     * @param id_user the id_user to set
     */
    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    /**
     * @return the id_annonce
     */
    public int getId_annonce() {
        return id_annonce;
    }

    /**
     * @param id_annonce the id_annonce to set
     */
    public void setId_annonce(int id_annonce) {
        this.id_annonce = id_annonce;
    }

    /**
     * @return the date_reservation
     */
    public String getDate_reservation() {
        return date_reservation;
    }

    /**
     * @param date_reservation the date_reservation to set
     */
    public void setDate_reservation(String date_reservation) {
        this.date_reservation = date_reservation;
    }

    /**
     * @return the date_expiration
     */
    public String getDate_expiration() {
        return date_expiration;
    }

    /**
     * @param date_expiration the date_expiration to set
     */
    public void setDate_expiration(String date_expiration) {
        this.date_expiration = date_expiration;
    }
    @Override
    public String toString(){
        return "{id_user: "+id_user+",id_annonce: "+id_annonce+",date reservation: "+date_reservation+",date expiration: "+date_expiration+"}";
        
    }
    
}
