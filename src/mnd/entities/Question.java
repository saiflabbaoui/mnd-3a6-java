/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.entities;

import java.util.Date;

/**
 *
 * @author Moataz
 */
public class Question {

    private int id;
    private int id_user;
    private String contenu;
    private String date_publication;
    private int nb_reponse;

    public Question(int id, int id_user, String date_publication, String contenu, int nb_reponse) {
        this.id = id;
        this.id_user = id_user;
        this.date_publication = date_publication;
        this.contenu = contenu;
        this.nb_reponse = nb_reponse;
    }

    public Question(int id, int id_user, String date_publication, String contenu) {
        this.id = id;
        this.id_user = id_user;
        this.date_publication = date_publication;
        this.contenu = contenu;
    }

    public Question(int id_user, String contenu, String date_publication) {
        this.id_user = id_user;
        this.contenu = contenu;
        this.date_publication = date_publication;
    }

    public Question(String contenu) {
        this.contenu = contenu;
    }
    

    public Question() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getDate_publication() {
        return date_publication;
    }

    public void setDate_publication(String date_publication) {
        this.date_publication = date_publication;
    }

    public int getNb_reponse() {
        return nb_reponse;
    }

    public void setNb_reponse(int nb_reponse) {
        this.nb_reponse = nb_reponse;
    }

    @Override
    public String toString() {
        return contenu ;
    }
    

    
  

}
