/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.services;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mnd.controllers.AuthentificationController;
import mnd.entities.Evenement;
import mnd.entities.Ratings;
import mnd.entities.User;
import mnd.interfaces.IEvenementService;
import mnd.utils.MyDBcon;

/**
 *
 * @author Steve
 */
public class EvenementServices implements IEvenementService {

    Connection connection;
    private PreparedStatement ps;
    private MyDBcon dataSource;

    public EvenementServices() {

        try {
            connection = MyDBcon.getInstance().getCnx();
        } catch (SQLException ex) {
            Logger.getLogger(EvenementServices.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void remove(Evenement e) {

        String reqSql = "DELETE FROM evenement WHERE id = ?";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement(reqSql);
            preparedStatement.setInt(1, e.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("error lors de la supression" + ex.getMessage());
        }

    }

    @Override
    public void add(Evenement ev) {
        String req = "INSERT INTO evenement (id_user,titre,nbr_participant,date,description,image_url,pays,longitude,latitude) VALUES (?,?,?,?,?,?,?,?,?)";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement(req);

            System.out.println(ev);
            preparedStatement.setInt(1, AuthentificationController.getInstance().getUser().getId());
            preparedStatement.setString(2, ev.getTitre());
            preparedStatement.setInt(3 ,ev.getNbr_participant());
            preparedStatement.setDate(4, ev.getDate());
            preparedStatement.setString(5, ev.getDescription());
            preparedStatement.setString(6, ev.getImage_url());
            preparedStatement.setString(7, ev.getPays());
            preparedStatement.setString(8, ev.getLongitude());
            preparedStatement.setString(9, ev.getLatitude());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Evenement ev, int i) {
        String req = "UPDATE evenement SET pays=?,titre =?,nbr_participant=?,date=?,description=?,image_url=? WHERE id=?";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement(req);
            preparedStatement.setString(1, ev.getPays());
            preparedStatement.setString(2, ev.getTitre());
            preparedStatement.setInt(3, ev.getNbr_participant());
            preparedStatement.setDate(4, ev.getDate());
            preparedStatement.setString(5, ev.getDescription());
            preparedStatement.setString(6, ev.getImage_url());
            preparedStatement.setInt(7, i);
            System.out.println(preparedStatement.toString());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Evenement> findAll() {

        Evenement ev = null;
        String req = "SELECT * FROM evenement";
        List<Evenement> evenements = new ArrayList<>();
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement(req);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Evenement evenement = new Evenement(rs.getInt("id"), rs.getString("pays"), rs.getString("titre"), rs.getInt("nbr_participant"), rs.getDate("date"), rs.getString("description"), rs.getString("image_url"));
                        Date datenow = new Date(System.currentTimeMillis());


                if ((evenement.getDate().after(datenow))||(evenement.getDate().equals(datenow))){
                    
                evenements.add(evenement);
                }
            }
            Collections.sort(evenements);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return evenements;

    }

    @Override
    public Evenement findById(int id) {
        String reqSql = "SELECT * FROM evenement WHERE evenement.id = ?";
        Evenement ev = new Evenement();
        PreparedStatement preparedStatement;
        try {
            ps = connection.prepareStatement(reqSql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ev.setId(rs.getInt("id"));
                ev.setId_user(rs.getInt("id_user"));
                ev.setPays(rs.getString("pays"));
                ev.setTitre(rs.getString("titre"));
                ev.setNbr_participant(rs.getInt("nbr_participant"));
                ev.setDate(rs.getDate("date"));
                ev.setDescription(rs.getString("description"));
                ev.setImage_url(rs.getString("image_url"));

                // Blob image = rs.getBlob("image_url");
                /* if (image != null) {
                    byte[] imagebytes = image.getBytes(1, (int) image.length());
                    ev.setImage_url(imagebytes);
                }*/
            }
        } catch (SQLException ex) {
            System.out.println("erreur " + ex.getMessage());
        }
        return ev;

    }

    @Override
    public void addRes(int id_evenement, int id_user) {
        String req = "INSERT INTO reservation_evenement (id_user,id_evenement,date_reservation) VALUES (?,?,?)";
         PreparedStatement ps;
        try {
            ps = connection.prepareStatement(req);
            ps.setInt(1, id_user);
            ps.setInt(2, id_evenement);
            ps.setDate(3, new java.sql.Date(Calendar.getInstance().getTime().getTime()));
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
    @Override
    public ArrayList<User> listRes(int id_evenement) {
        ArrayList users=new ArrayList();
        String req = "select * from reservation_evenement where id_evenement= ?";
         PreparedStatement ps;
        try {
            ps = connection.prepareStatement(req);
            ps.setInt(1, id_evenement);
            ResultSet rs=ps.executeQuery();
            while (rs.next()) {
                UserService us=new UserService();
               
                users.add(us.findUserById(rs.getInt("id_user")));
            }
            
           
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

     @Override
    public boolean testrating(int id_evenement,int id_user) {
        String reqSql = "SELECT * FROM rating_evenement WHERE id_evenement= ? and id_user= ? ";
        PreparedStatement ps;
        try {
            ps = connection.prepareStatement(reqSql);
            ps.setInt(1, id_evenement);
            ps.setInt(2, id_user);

            ResultSet rs = ps.executeQuery();
            System.out.println(ps.toString());
            if (rs.next()==true){
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvenementServices.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
        
}

    @Override
    public void rating(Ratings r) {
        String reqSql = "SELECT * FROM rating_evenement WHERE id_evenement= ? and id_user= ? ";
        PreparedStatement ps;
        try {
            ps = connection.prepareStatement(reqSql);
            ps.setInt(1, r.getId_evenement());
            ps.setInt(2, r.getId_user());

            ResultSet rs = ps.executeQuery();
            System.out.println(ps.toString());
            System.out.println(r);
            if (rs.next()==false) {
                     String req = "INSERT INTO rating_evenement (id_user,id_evenement,valeur) VALUES (?,?,?)";
                    PreparedStatement preparedStatement;
                    try {
                        preparedStatement = connection.prepareStatement(req);

                        preparedStatement.setInt(1, r.getId_user());
                        preparedStatement.setInt(2, r.getId_evenement());
                        preparedStatement.setDouble(3, r.getValeur());

                        preparedStatement.executeUpdate();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
            }
        } catch (SQLException ex) {
            Logger.getLogger(EvenementServices.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
