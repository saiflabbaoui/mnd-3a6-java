/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.services;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mnd.entities.Question;
import mnd.entities.User;
import mnd.interfaces.IQuestionService;
import mnd.utils.MyDBcon;

/**
 *
 * @author Moataz
 */
public class QuestionService implements IQuestionService {

    MyDBcon cnx;

    public QuestionService() {
        try {
            this.cnx = MyDBcon.getInstance();
        } catch (SQLException ex) {
            Logger.getLogger(QuestionService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void addQuestion(Question q) {
        String req = "INSERT INTO Question (`id_user`,`date_publication`, `contenu`) VALUES(?,?,?)";
        try {
            PreparedStatement ps = cnx.getCnx().prepareStatement(req);
            ps.setInt(1, q.getId_user());
            ps.setString(2, q.getDate_publication());
            ps.setString(3, q.getContenu());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(QuestionService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimerQuestion(Question q) {
        String req = "delete from `question` where id=?";
        try {
            PreparedStatement ps = cnx.getCnx().prepareStatement(req);
            ps.setInt(1, q.getId());
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(ReponseService.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    @Override
    public ArrayList<Question> afficherQuestion() {

        ArrayList<Question> list = new ArrayList<>();
        try {
            String req = "SELECT * from question";
            Statement stm = cnx.getCnx().createStatement();
            ResultSet rs = stm.executeQuery(req);
           
            while (rs.next()) {
                int id = rs.getInt("id");
                int id_user = rs.getInt("id_user");
                String date_publication = rs.getString("date_publication");
                String contenu = rs.getString("contenu");
                list.add(new Question(id, id_user, date_publication, contenu));
            }
        } catch (SQLException ex) {
            Logger.getLogger(QuestionService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    @Override
    public List<Question> RechercherQuestion(String c) {
        List<Question> list = new ArrayList<>();
        try {

            String req = "SELECT * from question  WHERE contenu LIKE '%" + c + "%'";
            PreparedStatement ps = cnx.getCnx().prepareStatement(req);
            //ps.setString(1, "%" + c + "%");
            ResultSet rs = ps.executeQuery(req);
            while (rs.next()) {
                int id = rs.getInt("id");
                int id_user = rs.getInt("id_user");
                String date_publication = rs.getString("date_publication");
                String contenu = rs.getString("contenu");
                list.add(new Question(id, id_user, date_publication, contenu));
            }
        } catch (SQLException ex) {
            Logger.getLogger(QuestionService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;

    }

    @Override
    public Question afficherMaQuestion(int id) {
        Question question = new Question();

        try {
            String req = "SELECT * FROM question where id =" + id;
            Statement stm = cnx.getCnx().createStatement();
            ResultSet rs = stm.executeQuery(req);
            while (rs.next()) {
                int id_user = rs.getInt("id_user");
                String date_publication = rs.getString("date_publication");
                String contenu = rs.getString("contenu");
                question = new Question(id, id_user, date_publication, contenu);
            }

        } catch (SQLException ex) {
            Logger.getLogger(QuestionService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return question;

    }
    @Override
    public ArrayList<Question> afficherMesQuestion(int id_user) {

        ArrayList<Question> list = new ArrayList<>();
        try {
            String req ="SELECT * FROM question where id_user= "+id_user;
            Statement stm = cnx.getCnx().createStatement();
            ResultSet rs = stm.executeQuery(req);
           
            while (rs.next()) {
                int id = rs.getInt("id");
                String date_publication = rs.getString("date_publication");
                String contenu = rs.getString("contenu");
                
                list.add(new Question(id, id_user, date_publication, contenu));
            }
        } catch (SQLException ex) {
            Logger.getLogger(QuestionService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }
    public ArrayList<Question> afficherContenuQuestion() {

        ArrayList<Question> list = new ArrayList<>();
        try {
            String req = "SELECT contenu from question";
            Statement stm = cnx.getCnx().createStatement();
            ResultSet rs = stm.executeQuery(req);
           
            while (rs.next()) {
                ;
                String contenu = rs.getString("contenu");
                list.add(new Question(contenu));
            }
        } catch (SQLException ex) {
            Logger.getLogger(QuestionService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }
}
