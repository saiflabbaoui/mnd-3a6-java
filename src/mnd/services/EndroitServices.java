/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.services;

import mnd.interfaces.EndroitServicesInterface;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mnd.entities.Commentaire;
import mnd.entities.Endroit;
import mnd.entities.User;
import mnd.utils.MyDBcon;

/**
 *
 * @author Ahmed
 */
public class EndroitServices implements EndroitServicesInterface {
     Connection cnx ;

    public EndroitServices() throws SQLException {
         cnx = MyDBcon.getInstance().getCnx();
    }
    
  
    
    @Override
    public void insertEndroit(Endroit e) {
     try {
             System.out.println("connexion établie");
             String req = "INSERT INTO endroit (`id_user`,`titre`, `image_url`, `description`,`pays`,`addresse`,`type`) VALUES ('" +e.getId_user() + "','" + e.getTitre()+"','"+e.getImage_url()+ "','"+ e.getDescription()+"','"+ e.getPays()+"','"+ e.getAddress()+ "','"+ e.getType()+"')";
             PreparedStatement pstm = cnx.prepareStatement(req);
             pstm.executeUpdate();
         } catch (SQLException ex) {
             Logger.getLogger(EndroitServices.class.getName()).log(Level.SEVERE, null, ex);
         }
    }

    @Override
    public boolean updateEndroit(Endroit endroit) {
       
        try {
            String sql = " UPDATE endroit set titre=?, pays=?, addresse=?, description=?, type=?, image_url=?  WHERE id="+ endroit.getId_endroit(); 
            PreparedStatement pstm = cnx.prepareStatement(sql);
            pstm.setString(1,endroit.getTitre());
            pstm.setString(2,endroit.getPays());
            pstm.setString(3,endroit.getAddress());
            pstm.setString(4,endroit.getDescription());
            pstm.setString(5,endroit.getType());
            pstm.setString(6,endroit.getImage_url());
            pstm.executeUpdate();
            
             return true;
         } catch (SQLException ex) {
             Logger.getLogger(EndroitServices.class.getName()).log(Level.SEVERE, null, ex);
         }
        return false; 
        
    }

    @Override
    public void deleteEndroit(Endroit endroit) {
       try{
           String sql = "DELETE from endroit where id = ?";
           PreparedStatement pstmt = cnx.prepareStatement(sql);
           pstmt.setInt(1,endroit.getId_endroit());
           pstmt.executeUpdate();
       } catch (SQLException ex) {
             Logger.getLogger(EndroitServices.class.getName()).log(Level.SEVERE, null, ex);
         }
    }

    @Override
    public List<Endroit> listAll() {
        String sql = "SELECT * from endroit order by id";
        List<Endroit> list = new ArrayList<>();
        try {
            Statement stmt = cnx.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
//            list = getFromreslt(rs);
            
            while (rs.next()) {
                int id_endroit = rs.getInt("id");
                int id_user = rs.getInt("id_user");
                String titre = rs.getString("titre");
                String description = rs.getString("description");
                String image_url = rs.getString("image_url");
                String pays = rs.getString("pays");
                String address = rs.getString("addresse");
                String type = rs.getString("type");
                list.add(new Endroit(id_endroit,id_user,titre,image_url,description,pays,address,type));
            }
           
        }  catch (SQLException ex) {
            Logger.getLogger(EndroitServices.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return list ; 
    }
    

    @Override
    public List<Endroit> getByUser(int id_user) {
        
        List<Endroit> list = new ArrayList<>();
        try{
            String sql = "SELECT * FROM endroit where id_user = "+ id_user ; 
            Statement stm = cnx.createStatement();
            ResultSet rs = stm.executeQuery(sql);
           while (rs.next()) {
               int id_endroit = rs.getInt("id");
                String titre = rs.getString("titre");
                String description = rs.getString("description");
                String image_url = rs.getString("image_url");
                String pays = rs.getString("pays");
                String address = rs.getString("addresse");
                String type = rs.getString("type");
                list.add(new Endroit(id_endroit,id_user,titre,image_url,description,pays,address,type));
            }
        }catch (SQLException ex) {
            Logger.getLogger(EndroitServices.class.getName()).log(Level.SEVERE, null, ex);
        } 
            return list ;
    }

    @Override
    public List<Endroit> getByCountry(String pays) {
        List<Endroit> list = new ArrayList<>() ;
        try {
            String sql = "SELECT * FROM endroit where pays ="+pays; 
            Statement stm2 = cnx.createStatement();
            ResultSet rs2 = stm2.executeQuery(sql);
             while(rs2.next()){
                int id_user = rs2.getInt("id_user");
                String titre = rs2.getString("titre");
                String description = rs2.getString("description");
                String image_url = rs2.getString("image_url");
                String paysBd = rs2.getString("pays");
                String address = rs2.getString("addresse");
                String type = rs2.getString("type");
                list.add(new Endroit(id_user,titre,description,image_url, paysBd, address,type));
            }
            
        } catch (SQLException ex) {
             Logger.getLogger(EndroitServices.class.getName()).log(Level.SEVERE, null, ex);
         }
        return list ; 
    }

    @Override
    public List<Commentaire> getAllComments(int endroit_id) {
        List<Commentaire> listComm = new ArrayList<>();
       try{
        String sql = "SELECT * FROM commentaire where id_endroit = " + endroit_id ;
        PreparedStatement pstmt = cnx.prepareStatement(sql) ;
        ResultSet rs = pstmt.executeQuery();
        while(rs.next()){
            int id_user = rs.getInt("id_user");
            String text = rs.getString("text");
            String date = rs.getString("date");
            listComm.add(new Commentaire(id_user,endroit_id, text,date));
        }
       } catch (SQLException ex) {
             Logger.getLogger(EndroitServices.class.getName()).log(Level.SEVERE, null, ex);
         }
       return listComm;
    }

    @Override
    public void noteEndroit(int note) {
        try{
            String sql= "UPDATE endroit set note = "+note ;
            Statement stm = cnx.createStatement();
            ResultSet rs = stm.executeQuery(sql);
        }catch (SQLException ex) {
             Logger.getLogger(EndroitServices.class.getName()).log(Level.SEVERE, null, ex);
         }
        
    }

    @Override
    public Endroit showEndroit(int endroit_id) {
        Endroit e = null ;
         try {
            String sql = "SELECT * from endroit where id = ?" ;
            PreparedStatement pstm = cnx.prepareStatement(sql);
            pstm.setInt(1, endroit_id);
            ResultSet rs = pstm.executeQuery();
            rs.next();
            int en_id = rs.getInt("id");
            int user_id = rs.getInt("id_user");
            String titre = rs.getString("titre");
            String description = rs.getString("description");
            String image_url = rs.getString("image_url");
            String pays = rs.getString("pays");
            String address = rs.getString("addresse");
            String type = rs.getString("type");
            e  = new Endroit(en_id, user_id,titre,image_url,description,pays,address,type);
            return e;

         } catch (SQLException ex) {
             Logger.getLogger(EndroitServices.class.getName()).log(Level.SEVERE, null, ex);
         }
         return e;
    }
    
    public boolean ifTitreExists(String titre)
    {
         try {
             String sql = "SELECT COUNT(*) FROM endroit where titre = ?" ;
             PreparedStatement pstm = cnx.prepareStatement(sql);
             pstm.setString(1, titre);
             ResultSet rs = pstm.executeQuery();
             rs.next();
             if(rs.getInt(1) != 0)
             {
                return true;
             }
             
         } catch (SQLException ex) {
             Logger.getLogger(EndroitServices.class.getName()).log(Level.SEVERE, null, ex);
         }
         return false ;
    }
    
    public List<Endroit> filterByTitle(String nom) {
        List<Endroit> list = new ArrayList<>() ;
        try {
            String sql1 = "SELECT * from lieux where titre like "+ nom + "%" ;
            Statement stm1 = cnx.createStatement();
            ResultSet rs = stm1.executeQuery(sql1);

            int id = rs.getInt("id_lieux");
            String sql = "SELECT * FROM endroit where lieux_id ="+id; 
            Statement stm2 = cnx.createStatement();
            ResultSet rs2 = stm2.executeQuery(sql);
             while(rs2.next()){
                int id_user = rs.getInt("id_user");
                String titre = rs2.getString("titre");
                String description = rs2.getString("description");
                String image_url = rs2.getString("image_url");
                String pays = rs2.getString("pays");
                String address = rs2.getString("addresse");
                String type = rs2.getString("type");
                list.add(new Endroit(id_user,titre,description,image_url, pays, address,type));
            }

        } catch (SQLException ex) {
             Logger.getLogger(EndroitServices.class.getName()).log(Level.SEVERE, null, ex);
         }
    return list ; 
}
    
    public User getUserByEndroit(Endroit e){
       // List<User> users = new ArrayList<>();
       User u = null  ;
        try {
            String sql = "SELECT * FROM fos_user where id =" + e.getId_user();
            PreparedStatement pstm = cnx.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            rs.next();
            int id =rs.getInt(1);
            String nom = rs.getString(2);
            String nomCan = rs.getString(3);
            String email = rs.getString(4);
            String emailCan= rs.getString(5);
            boolean enabled = rs.getBoolean(6);
            String salt = rs.getString(7);
            String password= rs.getString(8);
            Date lastLogin = rs.getDate(9);
            String token= rs.getString(10);
            Date passReqAt= rs.getDate(11);
            String role = rs.getString(12);
            String imageUrl= rs.getString(13); 
            String genre= rs.getString(14);
            int numTel= rs.getInt(15);
            String pays = rs.getString(16);
            int age = rs.getInt(17);
            u = new User(id, nom,nomCan,email,emailCan,enabled,salt,password,lastLogin,token,passReqAt,role,imageUrl,genre,numTel, pays,age) ;
           // users.add(e);
            
             
             
         } catch (SQLException ex) {
             Logger.getLogger(CommentaireServices.class.getName()).log(Level.SEVERE, null, ex);
         }
        return u ;
    }
    
    public void ajouterNote( int user_id, int endroit_id , double note){
         try {
             String sql = "INSERT INTO note_endroit (`id_user`, `id_endroit`, `note`) VALUES (?,?,? )" ;
             PreparedStatement pstm = cnx.prepareStatement(sql) ;
             pstm.setInt(1, user_id);
             pstm.setInt(2, endroit_id);
             pstm.setDouble(3, note);
             pstm.executeUpdate();
         } catch (SQLException ex) {
             Logger.getLogger(EndroitServices.class.getName()).log(Level.SEVERE, null, ex);
         }

    }
    
    public double getNoteGlobal(int id_endroit){
        double note = 0 ; 
        try {
             String sql = "SELECT AVG(note) from note_endroit where id_endroit = ?" ;
             PreparedStatement pstm = cnx.prepareStatement(sql) ;
             pstm.setInt(1, id_endroit);
             ResultSet rs = pstm.executeQuery();
             rs.next();
             note = rs.getDouble(1);
         } catch (SQLException ex) {
             Logger.getLogger(EndroitServices.class.getName()).log(Level.SEVERE, null, ex);
         }
         return note ; 
    }
  
}
