/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mnd.entities.Status;
import mnd.utils.MyDBcon;

/**
 *
 * @author Camilia
 */
public class StatusService {
   private Connection cnx;
    public StatusService()throws SQLException{
        cnx= MyDBcon.getInstance().getCnx();
    } 
    public int addStatus(Status s){
          try {
           String req = "insert into status(id_user, date_pub, contenu) values (?,?,?)";
            PreparedStatement pstm = cnx.prepareStatement(req,Statement.RETURN_GENERATED_KEYS);
            
            pstm.setInt(1, s.getId_user());
            pstm.setString(2, s.getDate_pub());
            pstm.setString(3, s.getContenu());
        
             pstm.executeUpdate();
            ResultSet result = pstm.getGeneratedKeys();
            if(result.next()) return result.getInt(1);
            
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
          return 0;
    }
    public void updateStatus(Status s){
           try {
           String req = "update status set id_user=?, date_pub=?, contenu=? where id=? ";
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setInt(1, s.getId_user());
            pstm.setString(2, s.getDate_pub());
            pstm.setString(3, s.getContenu());
             pstm.setInt(4, s.getId());
        
            pstm.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void deleteStatus(int id){
        try {
            String req = "delete from status where id =?";
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setInt(1, id);
            pstm.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    public List<Status> getAllStatus(){
        List<Status> status = new ArrayList<>();
        
        try {
            String req = "select * from status";
            PreparedStatement pstm = cnx.prepareStatement(req);
            ResultSet rs = pstm.executeQuery(req);
            while(rs.next()){
                Status s = new Status(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getString(4));
                status.add(s);
            }
            
          
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return status;
    }
public List<Status> getUserStatus(int id_user){
        List<Status> status = new ArrayList<>();
        
        try {
            String req = "select * from status where id_user =?";
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setInt(1,id_user);
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
                
                Status s = new Status(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getString(4));
                status.add(s);
            }
            
          
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return status;
    }

    public Status findStatusById(int id) {
        Status s = null;
        String req = "select * from status where id=?";

        try {
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setInt(1, id);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
            s = new Status(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getString(4));
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return s;

    }
}
