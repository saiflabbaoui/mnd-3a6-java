/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import mnd.entities.Annonce;
import mnd.entities.Vote_annonce;
import mnd.utils.MyDBcon;

/**
 *
 * @author kaisf
 */
public class Vote_annonceService {

    private Connection cnx;

    public Vote_annonceService() throws SQLException {
        cnx = MyDBcon.getInstance().getCnx();
    }

    public ArrayList<Vote_annonce> getVotesByIdUser(int id) {
        ArrayList<Vote_annonce> vote_annonce= new ArrayList<>();

        try {
            String req = "select * from vote_annonce where id_user=?";
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setInt(1, id);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                Vote_annonce u = new Vote_annonce(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getInt(4));
                vote_annonce.add(u);
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vote_annonce;
    }
    
    public void ajouterVote_annonce(Vote_annonce vote_annonce) {
         try {
             String req = "INSERT INTO `vote_annonce`( `id_user`,`id_voteur`,`vote`) VALUES (?,?,?)";
             PreparedStatement pstm = cnx.prepareStatement(req);
             pstm.setInt(1,vote_annonce.getId_user());
             pstm.setInt(2,vote_annonce.getId_voteur());
             pstm.setInt(3,vote_annonce.getVote());
             pstm.executeUpdate();
         } catch (SQLException ex) {
             Logger.getLogger(AnnonceService.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
     public void ModifierVote_annonce(Vote_annonce vote_annonce) {
        try {
            String req = "update vote_annonce set vote=? where id=?";
             PreparedStatement pstm = cnx.prepareStatement(req);
             pstm.setInt(1,vote_annonce.getVote());
             pstm.setInt(2,vote_annonce.getId());
             pstm.executeUpdate();
            pstm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AnnonceService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
