/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.services;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mnd.entities.Commentaire;
import mnd.entities.Endroit;
import mnd.entities.User;
import mnd.interfaces.ICommentaireServices;
import mnd.utils.MyDBcon;

/**
 *
 * 
 * @author Ahmed
 */
public class CommentaireServices implements ICommentaireServices {
     Connection cnx ;

    public CommentaireServices() throws SQLException {
        cnx = MyDBcon.getInstance().getCnx();
    }
    
     @Override
    public void insertComment(Commentaire c){
        try{
            String sql =  "INSERT INTO commentaire ( `id_user`,`id_endroit`,`date`, `text`) VALUES ( '"+c.getId_user() + "','" + c.getId_endroit()+"','" + c.getDate() +"','"+ c.getText() +  "')" ;
            PreparedStatement pstm = cnx.prepareStatement(sql);
            pstm.executeUpdate();
        } catch (SQLException ex) {
             Logger.getLogger(CommentaireServices.class.getName()).log(Level.SEVERE, null, ex);
         }
    }

    @Override
    public void updateComment(int endroit_id , String contenu) {
        try{
            String sql = "UPDATE commentaire set text = ? WHERE id_endroit = ? ";
            PreparedStatement pstm = cnx.prepareStatement(sql);
            pstm.setString(1,contenu);
            pstm.setInt(2, endroit_id);
            pstm.executeUpdate();
            System.out.println("update done !");
        }catch (SQLException ex) {
             Logger.getLogger(CommentaireServices.class.getName()).log(Level.SEVERE, null, ex);
         }
    }

    @Override
    public void deleteComment(Commentaire c) {
        try { 
            String sql = "DELETE FROM commentaire WHERE commentaire_id = " + c.getId_comm() ;
            PreparedStatement pstml = cnx.prepareStatement(sql);
            ResultSet rslt = pstml.executeQuery();
        } catch (SQLException ex) {
             Logger.getLogger(CommentaireServices.class.getName()).log(Level.SEVERE, null, ex);
         }
    }

    @Override
    public List<Commentaire> listAllComm(Endroit e) {
         String sql = "SELECT * from commentaire where id_endroit = " + e.getId_endroit();
         List<Commentaire> list = new ArrayList<>();
        try {
            Statement stmt = cnx.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                int id_user = rs.getInt("id_comm");
                int id_endroit = rs.getInt("id_endroit");
                String date = rs.getString("date");
                String text = rs.getString("text");
                list.add(new Commentaire(id_user, id_endroit, text,date));
            }
           
        }  catch (SQLException ex) {
            Logger.getLogger(EndroitServices.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return list ; 
    }
    
    public User getUserByComm(Commentaire c){
       // List<User> users = new ArrayList<>();
       User e = null  ;
        try {
             String sql = "SELECT * FROM fos_user where id =" + c.getId_user();
             PreparedStatement pstm = cnx.prepareStatement(sql);
             ResultSet rs = pstm.executeQuery();
             rs.next();
            int id =rs.getInt(1);
            String nom = rs.getString(2);
            String nomCan = rs.getString(3);
            String email = rs.getString(4);
            String emailCan= rs.getString(5);
            boolean enabled = rs.getBoolean(6);
            String salt = rs.getString(7);
            String password= rs.getString(8);
            Date lastLogin = rs.getDate(9);
            String token= rs.getString(10);
            Date passReqAt= rs.getDate(11);
            String role = rs.getString(12);
            String imageUrl= rs.getString(13); 
            String genre= rs.getString(14);
            int numTel= rs.getInt(15);
            String pays = rs.getString(16);
            int age = rs.getInt(17);
            e = new User(id, nom,nomCan,email,emailCan,enabled,salt,password,lastLogin,token,passReqAt,role,imageUrl,genre,numTel, pays,age) ;
           // users.add(e);
            
             
             
         } catch (SQLException ex) {
             Logger.getLogger(CommentaireServices.class.getName()).log(Level.SEVERE, null, ex);
         }
        return e ;
    }
    
     
}
