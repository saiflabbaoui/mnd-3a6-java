/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.services;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import mnd.entities.Reponse;
import mnd.entities.Vote;
import mnd.interfaces.IResponseService;
import mnd.utils.MyDBcon;

/**
 *
 * @author Moataz
 */
public class ReponseService implements IResponseService {

    MyDBcon cnx;

    public ReponseService() {
        try {
            this.cnx = MyDBcon.getInstance();
        } catch (SQLException ex) {
            Logger.getLogger(ReponseService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void addReponse(Reponse r) {
        String req = "INSERT INTO Reponse (`id_question`, `id_user`, `contenu`, `nb_votes`, `date_pub`) VALUES(?,?,?,?,?)";
        try {
            PreparedStatement ps = cnx.getCnx().prepareStatement(req);
            ps.setInt(1, r.getId_question());
            ps.setInt(2, r.getId_user());
            ps.setString(3, r.getContenu());
            ps.setInt(4, r.getNb_vote());
            ps.setString(5, r.getDate_pub());

            ps.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(QuestionService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modifierReponse(Reponse r) {
        try {
            String req = "UPDATE `reponse` SET `contenu`=? WHERE id=?";
            PreparedStatement ps = cnx.getCnx().prepareStatement(req);
            ps.setString(1, r.getContenu());
            ps.setInt(2, r.getId());

            ps.executeUpdate();
            

        } catch (SQLException ex) {
            Logger.getLogger(ReponseService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void supprimerReponse(Reponse r) {
        String req = "delete from `reponse` where id=?";
        try {
            PreparedStatement ps = cnx.getCnx().prepareStatement(req);
            ps.setInt(1, r.getId());
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(ReponseService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Reponse afficherMaReponse(int id) {
        Reponse reponse = new Reponse();

        try {
            String req = "SELECT * FROM reponse where id =" + id;
            Statement stm = cnx.getCnx().createStatement();
            ResultSet rs = stm.executeQuery(req);
            while (rs.next()) {
                int id_user = rs.getInt("id_user");
                int id_question = rs.getInt("id_question");
                String date_pub = rs.getString("date_pub");
                String contenu = rs.getString("contenu");
                int nb_votes = rs.getInt("nb_votes");
                reponse = new Reponse(id, id_question, id_user, contenu, nb_votes, date_pub);
            }

        } catch (SQLException ex) {
            Logger.getLogger(QuestionService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return reponse;

    }

    @Override
    public List<Reponse> afficherReponse(int i) {
        List<Reponse> list = new ArrayList<>();
        try {

            String req = "select * from reponse where id_question=" + i;
            PreparedStatement ps = cnx.getCnx().prepareStatement(req);

            ResultSet rs = ps.executeQuery(req);
            while (rs.next()) {
                int id = rs.getInt("id");
                int id_question = rs.getInt("id_question");
                int id_user = rs.getInt("id_user");
                String contenu = rs.getString("contenu");
                int nb_votes = rs.getInt("nb_votes");
                String date_pub = rs.getString("date_pub");

                list.add(new Reponse(id, id_question, id_user, contenu, nb_votes, date_pub));
            }

        } catch (SQLException ex) {
            Logger.getLogger(ReponseService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public void voteReponse(int id_user, int id_reponse) {
        String req = "INSERT INTO vote (`id_reponse`, `id_user`) VALUES(?,?)";
        try {
            PreparedStatement ps = cnx.getCnx().prepareStatement(req);
            ps.setInt(2, id_user);
            ps.setInt(1, id_reponse);

            ps.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(QuestionService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void unvoteReponse(int id_user, int id_reponse) {
        String req = "delete from `vote` where id_user=? AND id_reponse=?";
        try {
            PreparedStatement ps = cnx.getCnx().prepareStatement(req);
            ps.setInt(1, id_user);
            ps.setInt(2, id_reponse);
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(ReponseService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public int voteExist(int id_user, int id_reponse) {
        int count = 0;
        try {
            String req = "select Count(id_user) as count from `vote` where id_user=" + id_user + " AND id_reponse=" + id_reponse;
            PreparedStatement ps = cnx.getCnx().prepareStatement(req);
            ResultSet rs = ps.executeQuery(req);
            while (rs.next()) {
                count = rs.getInt("count");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReponseService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    @Override
    public int countVote(int id_reponse) {
        int count = 0;
        try {
            String req = "select Count(id_reponse) as nb_votes from `vote` where id_reponse=" + id_reponse;
            PreparedStatement ps = cnx.getCnx().prepareStatement(req);
            ResultSet rs = ps.executeQuery(req);
            while (rs.next()) {
                count = rs.getInt("vote");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReponseService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }

    public List<Vote> AfficherNbVote(int i) {
        List<Vote> list = new ArrayList<>();
        try {

            String req = "select * from vote where id_reponse="+i;
            PreparedStatement ps = cnx.getCnx().prepareStatement(req);

            ResultSet rs = ps.executeQuery(req);
            while (rs.next()) {
                int id_reponse = rs.getInt("id_reponse");
                int id_user = rs.getInt("id_user");
                list.add(new Vote(id_user,id_reponse));
            }

        } catch (SQLException ex) {
            Logger.getLogger(ReponseService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}
