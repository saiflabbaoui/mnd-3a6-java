/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.services;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mnd.entities.User;
import mnd.interfaces.IUserService;
import mnd.utils.MyDBcon;

/**
 *
 * @author Camilia
 */
public class UserService implements IUserService {

    private Connection cnx;

    public UserService() throws SQLException {
        cnx = MyDBcon.getInstance().getCnx();
    }

    @Override
    public void addUser(User u) {

        try {
            String req = "insert into fos_user(username, username_canonical, email, email_canonical, enabled,  salt, password, last_login, confirmation_token, password_requested_at, roles, photo_membre,  genre, phone_number, country, age) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setString(1, u.getUsername());
            pstm.setString(2, u.getUsername_canonical());
            pstm.setString(3, u.getEmail());
            pstm.setString(4, u.getEmail_canonical());
            pstm.setBoolean(5, u.getEnabled());
            pstm.setString(6, u.getSalt());
            pstm.setString(7, u.getPassword());
            pstm.setDate(8, (Date) u.getLastLogin());
            pstm.setString(9, u.getConfirmationToken());
            pstm.setDate(10, (Date) u.getPasswordRequestedAt());
            pstm.setString(11, u.getRoles());
            pstm.setString(12, u.getPhoto_membre());
            pstm.setString(13, u.getGenre());
            pstm.setInt(14, u.getPhone_number());
            pstm.setString(15, u.getCountry());
            pstm.setInt(16, u.getAge());
            pstm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void updateUser(User u) {
        try {
            String req = "update fos_user set country=?, username=?, age=?, phone_number=?, email=? where id=?";
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setString(1, u.getCountry());
            pstm.setString(2,u.getUsername());
             pstm.setInt(3, u.getAge());
            pstm.setInt(4, u.getPhone_number());
            pstm.setString(5, u.getEmail());
            
       
            pstm.setInt(6, u.getId());
            pstm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void updateUserEnabled(User u){
               try {
            String req = "update fos_user set enabled=? where id=?";
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setBoolean(1, u.getEnabled());
   
            pstm.setInt(2, u.getId());
            pstm.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void deleteUser(int id) {
        try {
            String req = "delete from fos_user where id =?";
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setInt(1, id);
            pstm.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();

        try {
            String req = "select * from fos_user";
            PreparedStatement pstm = cnx.prepareStatement(req);
            ResultSet rs = pstm.executeQuery(req);
            while (rs.next()) {
                User u = new User(rs.getInt(1),rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getBoolean(6), rs.getString(7), rs.getString(8), rs.getDate(9), rs.getString(10), rs.getDate(11), rs.getString(12), rs.getString(13), rs.getString(14),rs.getInt(15),rs.getString(16),rs.getInt(17));
                users.add(u);
            }

        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return users;
    }
    @Override
    public ArrayList<User> getUsersInfo(){
        ArrayList<User> users = new ArrayList<>();
       
        try { 
            String req = "select * from fos_user";
            PreparedStatement pstm;
            pstm = cnx.prepareStatement(req);
            ResultSet rs = pstm.executeQuery(req);
            while(rs.next()){
                String name = rs.getString("username");
                User u = new User(name);
                users.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return users;
        
    }

    @Override
    public User findUserById(int id) {
        User u = null;
        String req = "select * from fos_user where id=?";

        try {
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setInt(1, id);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
            u = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getBoolean(6), rs.getString(7), rs.getString(8), rs.getDate(9), rs.getString(10), rs.getDate(11), rs.getString(12), rs.getString(13), rs.getString(14),rs.getInt(15),rs.getString(16),rs.getInt(17));
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return u;

    }

    @Override
    public User findUserByUsername(String username) {
        User u = null;
        String req = "select * from fos_user where username=?";
        try {
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setString(1, username);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
           u = new User(rs.getInt(1),rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getBoolean(6), rs.getString(7), rs.getString(8), rs.getDate(9), rs.getString(10), rs.getDate(11), rs.getString(12), rs.getString(13), rs.getString(14),rs.getInt(15),rs.getString(16),rs.getInt(17));
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return u;

    }
     @Override
    public User findUserByEmail(String email) {
        User u = null;
        String req = "select * from fos_user where email=?";
        try {
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setString(1, email);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
           u = new User(rs.getInt(1),rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getBoolean(6), rs.getString(7), rs.getString(8), rs.getDate(9), rs.getString(10), rs.getDate(11), rs.getString(12), rs.getString(13), rs.getString(14),rs.getInt(15),rs.getString(16),rs.getInt(17));
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return u;

    }


}
