/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.services;

/**
 *
 * @author kaisf
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import mnd.entities.Annonce;
import mnd.utils.MyDBcon;

/**
 *
 * @author Wadii Chamakhi
 */
public class AnnonceService {

    Connection cnx;

    public AnnonceService() throws SQLException {
        cnx = MyDBcon.getInstance().getCnx();

    }

    public void ajouterAnnonce(Annonce annonce) {
        try {
            String req = "INSERT INTO `annonce`( `id_user`,`date_annonce`,`description`,`nb_personne`,`sexe`,`age`,`date_expiration`,`image_url`,`status`,`pays`) VALUES (?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setString(1, Integer.toString(annonce.getId_user()));
            pstm.setString(2, annonce.getDate_annonce());
            pstm.setString(3, annonce.getDescription());
            pstm.setString(4, Integer.toString(annonce.getNb_personne()));
            pstm.setString(5, annonce.getSexe());
            pstm.setString(6, Integer.toString(annonce.getAge()));
            pstm.setString(7, annonce.getDate_expiration());
            pstm.setString(8, annonce.getImage_url());
            pstm.setString(9, annonce.getStatus());
            pstm.setString(10, annonce.getPays());
            pstm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AnnonceService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void ModifierAnnonce(Annonce annonce) {
        try {
            String req = "update `annonce` set `date_annonce`=?,`description`=?,`nb_personne`=?,`sexe`=?,`age`=?,`date_expiration`=?,`status`=?,`pays`=?,`image_url`=? where id=?";
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setString(1, annonce.getDate_annonce());
            pstm.setString(2, annonce.getDescription());
            pstm.setInt(3, annonce.getNb_personne());
            pstm.setString(4, annonce.getSexe());
            pstm.setInt(5, annonce.getAge());
            pstm.setString(6, annonce.getDate_expiration());
            pstm.setString(7, annonce.getStatus());
            pstm.setString(8, annonce.getPays());
            pstm.setString(9, annonce.getImage_url());
            pstm.setInt(10, annonce.getId());
            pstm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AnnonceService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void supprimerAnnonceParid(int id) throws SQLException {
        try {
            String req = "DELETE FROM `annonce` WHERE id=?";
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setString(1, Integer.toString(id));
            pstm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AnnonceService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Annonce> getTousAnnonce() throws SQLException {
        ArrayList<Annonce> retour = new ArrayList<>();
        Statement stm = cnx.createStatement();
        String req = "SELECT * FROM annonce";
        ResultSet resultat = stm.executeQuery(req);
        while (resultat.next()) {
            int id = Integer.parseInt(resultat.getString("id"));
            int id_user = Integer.parseInt(resultat.getString("id_user"));
            String date_annonce = resultat.getString("date_annonce");
            String description = resultat.getString("description");
            int nb_personne = Integer.parseInt(resultat.getString("nb_personne"));
            String sexe = resultat.getString("sexe");
            int age = Integer.parseInt(resultat.getString("age"));
            String date_expiration = resultat.getString("date_expiration");
            String image_url = resultat.getString("image_url");
            String status = resultat.getString("status");
            String pays = resultat.getString("pays");

            retour.add(new Annonce(id, id_user, date_annonce, description, nb_personne, sexe, age, date_expiration, image_url, status, pays));

        }

        return retour;
    }

    public ArrayList<Annonce> getTousAnnonceLibre() throws SQLException {
        ArrayList<Annonce> retour = new ArrayList<>();
        Statement stm = cnx.createStatement();
        String req = "SELECT * FROM annonce where status='l'";
        ResultSet resultat = stm.executeQuery(req);
        while (resultat.next()) {
            int id = Integer.parseInt(resultat.getString("id"));
            int id_user = Integer.parseInt(resultat.getString("id_user"));
            String date_annonce = resultat.getString("date_annonce");
            String description = resultat.getString("description");
            int nb_personne = Integer.parseInt(resultat.getString("nb_personne"));
            String sexe = resultat.getString("sexe");
            int age = Integer.parseInt(resultat.getString("age"));
            String date_expiration = resultat.getString("date_expiration");
            String image_url = resultat.getString("image_url");
            String status = resultat.getString("status");
            String pays = resultat.getString("pays");
            retour.add(new Annonce(id, id_user, date_annonce, description, nb_personne, sexe, age, date_expiration, image_url, status, pays));

        }

        return retour;
    }

    public ArrayList<Annonce> getAnnoncePardescription(String d) throws SQLException {
        ArrayList<Annonce> retour = new ArrayList<>();
        String req = "SELECT * FROM `annonce` where description like ? and status='l' ";
        PreparedStatement pstm = cnx.prepareStatement(req);
        pstm.setString(1, "%" + d + '%');
        ResultSet resultat = pstm.executeQuery();
        while (resultat.next()) {
            int id = Integer.parseInt(resultat.getString("id"));
            int id_user = Integer.parseInt(resultat.getString("id_user"));
            String date_annonce = resultat.getString("date_annonce");
            String description = resultat.getString("description");
            int nb_personne = Integer.parseInt(resultat.getString("nb_personne"));
            String sexe = resultat.getString("sexe");
            int age = Integer.parseInt(resultat.getString("age"));
            String date_expiration = resultat.getString("date_expiration");
            String image_url = resultat.getString("image_url");
            String status = resultat.getString("status");
            String pays = resultat.getString("pays");
            retour.add(new Annonce(id, id_user, date_annonce, description, nb_personne, sexe, age, date_expiration, image_url, status, pays));
        }

        return retour;
    }

    public ArrayList<Annonce> getAnnonceParuserid(int i) throws SQLException {
        ArrayList<Annonce> retour = new ArrayList<>();
        String req = "SELECT * FROM `annonce` where id_user=? and status='l' ";
        PreparedStatement pstm = cnx.prepareStatement(req);
        pstm.setString(1, Integer.toString(i));
        ResultSet resultat = pstm.executeQuery();
        while (resultat.next()) {
            int id = Integer.parseInt(resultat.getString("id"));
            int id_user = Integer.parseInt(resultat.getString("id_user"));
            String date_annonce = resultat.getString("date_annonce");
            String description = resultat.getString("description");
            int nb_personne = Integer.parseInt(resultat.getString("nb_personne"));
            String sexe = resultat.getString("sexe");
            int age = Integer.parseInt(resultat.getString("age"));
            String date_expiration = resultat.getString("date_expiration");
            String image_url = resultat.getString("image_url");
            String status = resultat.getString("status");
            String pays = resultat.getString("pays");
            retour.add(new Annonce(id, id_user, date_annonce, description, nb_personne, sexe, age, date_expiration, image_url, status, pays));

        }

        return retour;
    }

    public ArrayList<Annonce> getAnnonceParpays(String p) throws SQLException {
        ArrayList<Annonce> retour = new ArrayList<>();
        String req = "select * from annonce where pays=? and status='l'";
        PreparedStatement pstm = cnx.prepareStatement(req);
        pstm.setString(1, p);
        ResultSet resultat = pstm.executeQuery();
        while (resultat.next()) {
            int id = Integer.parseInt(resultat.getString("id"));
            int id_user = Integer.parseInt(resultat.getString("id_user"));
            String date_annonce = resultat.getString("date_annonce");
            String description = resultat.getString("description");
            int nb_personne = Integer.parseInt(resultat.getString("nb_personne"));
            String sexe = resultat.getString("sexe");
            int age = Integer.parseInt(resultat.getString("age"));
            String date_expiration = resultat.getString("date_expiration");
            String image_url = resultat.getString("image_url");
            String status = resultat.getString("status");
            String pays = resultat.getString("pays");
            retour.add(new Annonce(id, id_user, date_annonce, description, nb_personne, sexe, age, date_expiration, image_url, status, pays));
        }
        return retour;
    }

    public Annonce getAnnonceByid(int annonceid) throws SQLException {
        String req = "select * from annonce where id=?";
        PreparedStatement pstm = cnx.prepareStatement(req);
        pstm.setInt(1, annonceid);
        ResultSet resultat = pstm.executeQuery();
        resultat.next();
        int id = Integer.parseInt(resultat.getString("id"));
        int id_user = Integer.parseInt(resultat.getString("id_user"));
        String date_annonce = resultat.getString("date_annonce");
        String description = resultat.getString("description");
        int nb_personne = Integer.parseInt(resultat.getString("nb_personne"));
        String sexe = resultat.getString("sexe");
        int age = Integer.parseInt(resultat.getString("age"));
        String date_expiration = resultat.getString("date_expiration");
        String image_url = resultat.getString("image_url");
        String status = resultat.getString("status");
        String pays = resultat.getString("pays");
        return new Annonce(id, id_user, date_annonce, description, nb_personne, sexe, age, date_expiration, image_url, status, pays);
    }

}
